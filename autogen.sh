#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="gxtar"

(test -f $srcdir/configure.in \
  && test -f $srcdir/gxtar.c) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level gxtar directory"
    exit 1
}

. $srcdir/macros/autogen.sh
