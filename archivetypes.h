/* gxTar - Gnomified Archive Frontend
 * Copyright (C) 1998 Chris Rogers
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option) 
 * any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  
 * 02111-1307, USA.
 */

/* archive.h - contains basic archive functions */
/* right now we must interact with the cli programs, perhaps someday we can implement the (de)compression ourslef */

#ifndef __ARCHIVETYPES_H__
#define __ARCHIVETYPES_H__


#define KNOWNEXT  7 

#define COLNUM 6

struct archive_type {

  gchar *mimetype;
  int (*readit) (gchar *);
  int (*extractit) (gchar *);
  int (*addit) (GtkFileSelection *); 
  int (*deleteit) (); 
  gchar * (*viewit) (gchar *);
}  archList[KNOWNEXT];

static const int GZ = 0;
static const int TAR = 2;
static const int TGZ = 1;
static const int ZIP = 3;
static const int BZ2 = 4;
static const int TBZ2 = 5;
static const int ARJ = 6;

gchar *  splitNamePath(gchar * src, int tst);

#endif /* __ARCHIVETYPES_H__ */
