/* gxTar - Gnomified Archive Frontend
 * Copyright (C) 1998 Chris Rogers
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option) 
 * any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  
 * 02111-1307, USA.
 */

#include "toolbar.h"
#include "archive.h"


/* Button events */

void archive_open (GtkWidget *widget, gpointer *data) {
  openfileArchive();
}

void archive_close (GtkWidget *widget, gpointer *data) {
  closeArchive();
}

void archive_extract (GtkWidget *widget, gpointer *data){
  extractArchive();
}

void archive_add (GtkWidget *widget, gpointer *data){
  addArchive();
}

void archive_view (GtkWidget *widget, gpointer *data){
  viewFile();
}

void archive_delete (GtkWidget *widget, gpointer *data){
  deleteArchive();
}

enum {
  TARGET_URI_LIST,
};


static void toolbar_drop(GtkWidget *widget, GdkDragContext *context,
                       gint x, gint y, GtkSelectionData *selection_data,
                       guint info, guint time, gpointer data) {
  GList *names, *list;
  
  if(arch->OpenFile != NULL) {
    closeArchive();
  }


  switch(info) {
  case TARGET_URI_LIST:
    list = names = gnome_uri_list_extract_filenames (selection_data->data);

    if (names) {
      
      arch->OpenFile = g_malloc(sizeof(gchar)*(strlen((gchar*)names->data)+1));
      strcpy(arch->OpenFile,(gchar *)names->data);
      openArchive();
    }

    gnome_uri_list_free_strings(list);
    break;
  default:
  }
}





void createToolbar(GtkWidget *app)
{

static GtkTargetEntry drop_types [] = {
    { "text/uri-list", 0, TARGET_URI_LIST}
  };
  static gint n_drop_types = sizeof (drop_types) / sizeof(drop_types[0]);


  GnomeUIInfo toolbar[] = {
    GNOMEUIINFO_ITEM_STOCK(N_("Open"), N_("Open archive"), archive_open, GNOME_STOCK_PIXMAP_OPEN),
    GNOMEUIINFO_ITEM_STOCK(N_("Close"), N_("Close archive"), archive_close, GNOME_STOCK_PIXMAP_CLOSE),
    GNOMEUIINFO_ITEM_STOCK(N_("Extract"), N_("Extract from archive"), archive_extract,GNOME_STOCK_PIXMAP_CONVERT),
    GNOMEUIINFO_ITEM_STOCK(N_("View"), N_("View file"), archive_view,GNOME_STOCK_PIXMAP_SEARCH),
    GNOMEUIINFO_ITEM_STOCK(N_("Add"), N_("Add file"),archive_add,GNOME_STOCK_PIXMAP_COPY),
    GNOMEUIINFO_ITEM_STOCK(N_("Delete"), N_("Delete file(s)"),archive_delete,GNOME_STOCK_PIXMAP_CUT),
    GNOMEUIINFO_END
  };
  
  gnome_app_create_toolbar(GNOME_APP(app), toolbar);
  
  
  gtk_drag_dest_set (app,
                     GTK_DEST_DEFAULT_MOTION |
                     GTK_DEST_DEFAULT_HIGHLIGHT |
                     GTK_DEST_DEFAULT_DROP,
                     drop_types, n_drop_types,
                     GDK_ACTION_COPY);

  gtk_signal_connect (GTK_OBJECT (app), "drag_data_received",
                      GTK_SIGNAL_FUNC(toolbar_drop), NULL);
}
