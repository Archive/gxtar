/* gxTar - Gnomified Archive Frontend
 * Copyright (C) 1998 Chris Rogers
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option) 
 * any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  
 * 02111-1307, USA.
 */

#include "error.h"


void ShowError(gchar *emsg) {


  GtkWidget * dialog;
  
  dialog = gnome_ok_dialog (emsg);

  /*dialog = gnome_message_box_new(emsg, GNOME_MESSAGE_BOX_QUESTION, 
    GNOME_STOCK_BUTTON_OK, NULL);*/
  gtk_window_set_modal(GTK_WINDOW(dialog),TRUE);
  gtk_widget_show(dialog);
}
