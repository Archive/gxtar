/* gxTar - Gnomified Archive Frontend
 * Copyright (C) 1998 Chris Rogers
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option) 
 * any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  
 * 02111-1307, USA.
 */

#include "zip.h"


int zip_read(gchar * fle) {

  gchar *text[COLNUM];
  gchar tmp[9][1024];
  gchar trash[1024];
  FILE * fl;
  gchar *cmd;
  int i,j;

  cmd = g_malloc(sizeof(gchar)*(12+strlen(fle)));
  sprintf(cmd, "unzip -Z \"%s\"", fle);
  
  if ((fl = popen(cmd,"r")) == NULL) {
    return -1 ;
  }
  g_free(cmd);

  for  (i = 0; i < 1; i++) fgets(trash, 1023, fl);

  j = 0;
  while (!feof(fl))  
    {
      fscanf(fl,"%s %s %s %s %s %s %s %s",tmp[0],tmp[1],tmp[2],tmp[3],tmp[4],tmp[5],tmp[6],tmp[7]);
      text[3] = tmp[0];
      text[2] = tmp[3];
      text[1] = tmp[6];
      text[4] = NULL;

      fgets(tmp[8],1023,fl); /*workaround for spaces in last string*/
      tmp[8][strlen(tmp[8])-1] = '\0'; /*now remove the last newline*/

      text[0] = splitNamePath(tmp[8]+1,0); /*skips the space*/
      text[5] = splitNamePath(tmp[8]+1,1);
      
      gtk_clist_append( GTK_CLIST (arch->listFiles) , text);
      j++;
      g_free(text[0]);
      g_free(text[5]);
}
  gtk_clist_remove (GTK_CLIST (arch->listFiles),j-1); /*we get an extra line for some reason???*/
  gtk_clist_remove (GTK_CLIST (arch->listFiles),j-2); /*get rid of the totals line*/
  pclose(fl);
  return 0; 
}


int  zip_extract(gchar * dir){

  GList *selection;
  gint index;
  gchar *text;
  gchar *cmd;
  gint sze;
  int result;
  int nullnum;

  selection = GTK_CLIST(arch->listFiles)->selection;

  if (debug_off) nullnum = 15; else nullnum = 0;
  
  if (selection == NULL) {
    cmd = g_malloc(sizeof(gchar)*(15+strlen(dir)+strlen(arch->OpenFile)+nullnum));
    sprintf(cmd, "unzip \"%s\" -d \"%s\"", arch->OpenFile, dir);
  }
  else{
    sze = 15+strlen(dir)+strlen(arch->OpenFile);
    
    while(selection) {
      index = (gint) selection->data;
      /* strcat(cmd," \""); */ sze = sze +2;
      gtk_clist_get_text(GTK_CLIST(arch->listFiles), index, COLNUM-1, &text);
      /*strcat(cmd,text); */ sze = sze + strlen(text);
      gtk_clist_get_text(GTK_CLIST(arch->listFiles), index, 0, &text);
      /*strcat(cmd,text); */ sze = sze + strlen(text);
      /*strcat(cmd,"\"");*/ sze = sze + 1;
      selection= selection->next;
    }

    cmd = g_malloc(sizeof(gchar)*sze);
    sprintf(cmd,"unzip \"%s\" -d \"%s\"", arch->OpenFile, dir);

    selection = GTK_CLIST(arch->listFiles)->selection;
    
    while(selection) {
      index = (gint) selection->data;
      strcat(cmd," \"");
      gtk_clist_get_text(GTK_CLIST(arch->listFiles), index, COLNUM-1, &text);
      strcat(cmd,text); 
      gtk_clist_get_text(GTK_CLIST(arch->listFiles), index, 0, &text);
      strcat(cmd,text); 
      strcat(cmd,"\"");
      selection= selection->next;
    }
  }

  if (debug_off) strcat(cmd, " >> /dev/null");

  result = system(cmd);
  g_free(cmd);
  return result;
}


int zip_add(GtkFileSelection *fs){
  
  gchar *cmd;
  int result;
  int nullnum;
  GList *selection;
  gint index;
  gchar *text;
  gint sze;

  selection = GTK_CLIST(fs->file_list)->selection;

  if (debug_off) nullnum = 15; else nullnum = 0;
  
  if (selection == NULL) {
    cmd = g_malloc(sizeof(gchar)*(17+strlen(gtk_file_selection_get_filename (GTK_FILE_SELECTION (fs)))+strlen(arch->OpenFile)+nullnum));
    sprintf(cmd, "zip \"%s\" \"%s/\"*", arch->OpenFile, gtk_file_selection_get_filename (GTK_FILE_SELECTION (fs)));
  }
  else{
    sze = 15+strlen(arch->OpenFile);
    
    while(selection) {
      index = (gint) selection->data;
      /* strcat(cmd," \""); */ sze = sze +2;
      /*strcat(cmd,text); */ sze = sze + strlen(LastODir);
      gtk_clist_get_text(GTK_CLIST(fs->file_list), index, 0, &text);
      /*strcat(cmd,text); */ sze = sze + strlen(text);
      /*strcat(cmd,"\"");*/ sze = sze + 1;
      selection= selection->next;
    }

    cmd = g_malloc(sizeof(gchar)*sze);
    sprintf(cmd,"zip \"%s\" ", arch->OpenFile);

    selection = GTK_CLIST(fs->file_list)->selection;
    
    while(selection) {
      index = (gint) selection->data;
      strcat(cmd," \"");
      strcat(cmd,LastODir); 
      gtk_clist_get_text(GTK_CLIST(fs->file_list), index, 0, &text);
      strcat(cmd,text); 
      strcat(cmd,"\"");
      selection= selection->next;
    }
    
  }

  if (debug_off) strcat(cmd, " >> /dev/null");

  result = system(cmd);
  g_free(cmd);
  return result;
}


int zip_delete(gchar *fle){

  GList *selection;
  gint index;
  gchar *text;
  gchar *cmd;
  gint sze;
  int result;

  selection = GTK_CLIST(arch->listFiles)->selection;

  if (debug_off) sze = 15; else sze = 0;

  sze = sze + 10 + strlen(arch->OpenFile);

  while(selection) {
    index = (gint) selection->data;
    /*strcat(cmd," ");*/ sze = sze +2;
    gtk_clist_get_text(GTK_CLIST(arch->listFiles), index, COLNUM-1, &text);
    /*strcat(cmd,text); */ sze = sze +strlen(text);
    gtk_clist_get_text(GTK_CLIST(arch->listFiles), index, 0, &text);
    /*strcat(cmd,text); */ sze = sze +strlen(text);
    selection= selection->next;
  }

  cmd = g_malloc(sizeof(gchar)*sze);

  sprintf(cmd,"zip -d \"%s\"", arch->OpenFile);

  selection = GTK_CLIST(arch->listFiles)->selection;

  while(selection) {
    index = (gint) selection->data;
    strcat(cmd," ");
    gtk_clist_get_text(GTK_CLIST(arch->listFiles), index, COLNUM-1, &text);
    strcat(cmd,text); 
    gtk_clist_get_text(GTK_CLIST(arch->listFiles), index, 0, &text);
    strcat(cmd,text); 
    selection= selection->next;
  }

  if (debug_off) strcat(cmd, " >> /dev/null");
  
  result = system(cmd); 
  g_free(cmd);
  return result;
}



gchar *  zip_view( gchar *fle){
  gchar * cmd;
  cmd = g_malloc(sizeof(gchar)*(15+strlen(fle)+strlen(arch->OpenFile)));
  sprintf(cmd, "unzip -p \"%s\" \"%s\"", arch->OpenFile, fle);
  return cmd;
}

