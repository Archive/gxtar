/* gxTar - Gnomified Archive Frontend
 * Copyright (C) 1998 Chris Rogers
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option) 
 * any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  
 * 02111-1307, USA.
 */

#include "gz.h"


int gz_read(gchar * fle) {

  gchar *text[COLNUM];
  gchar tmp[9][1024];
  gchar trash[1024];
  FILE * fl;
  gchar *cmd;
  int i,j;
  gchar *ext;
  gchar *savedot;
 
 /*ok first we want to check for the possibility of a tar.gz file.*/
   ext = strrchr(fle, '.');
  savedot = ext;
  ext[0] = '\0';
  if ((ext = strrchr(fle, '.')) != NULL) /*look for the next to last .*/
      if (strcmp(ext, ".tar")==0) {
	savedot[0] = '.'; /* put it back */
	arch->filetype = TGZ;
	return archList[arch->filetype].readit(fle);
      }
  savedot[0] = '.'; /* put it back */

  /* we are a normal .gz, lets go */
  cmd = g_malloc(sizeof(gchar)*(8+strlen(fle)+strlen(gunzipl) ));
  sprintf(cmd, "%s -lv \"%s\"",gunzipl, fle);
 

  if ((fl = popen(cmd,"r")) == NULL) {
    return -1 ;
  }
  g_free(cmd);

  for  (i = 0; i < 1; i++) fgets(trash, 1023, fl);

  j = 0;
  while (!feof(fl))  
    {
      fscanf(fl,"%s %s %s %s %s %s %s %s %s",tmp[0],tmp[1],tmp[2],tmp[3],tmp[4],tmp[5],tmp[6],tmp[7],tmp[8]);
      text[1] = tmp[2];
      text[2] = tmp[6];
      text[3] = NULL;
      text[4] = tmp[7];

      tmp[8][strlen(tmp[8])-1] = '\0'; /*get rid of the newline*/

      text[0] = splitNamePath(tmp[8]+1,0);
      text[5] = splitNamePath(tmp[8]+1,1);
      
      gtk_clist_append( GTK_CLIST (arch->listFiles) , text);
      j++;
      g_free(text[0]);
      g_free(text[5]);
}
  gtk_clist_remove (GTK_CLIST (arch->listFiles),j-1); /*we get an extra line for some reason???*/

  pclose(fl);
  return 0; 
}

int  gz_extract(gchar * dir){


  gchar *name;
  gchar *cmd;
  int result;

  gtk_clist_get_text(GTK_CLIST(arch->listFiles), 0, 0, &name);
  cmd = g_malloc(sizeof(gchar)*(19+strlen(dir)+strlen(arch->OpenFile)+strlen(name)+strlen(gzipl)  ));
  sprintf(cmd, "cd \"%s\"; %s -c \"%s\" > \"%s\"", dir, gzipl, arch->OpenFile, name);
  result = system(cmd);
  g_free(cmd);
 return result;
}

int gz_add(GtkFileSelection *fs){return -1;} 

int gz_delete(gchar * cmd, GList * sltn){cmd[0] = '\0'; return -1;}

gchar * gz_view(gchar *fle){
  gchar * cmd;
  cmd = g_malloc(sizeof(gchar)*(7+strlen(arch->OpenFile)+strlen(gunzipl)));
  sprintf(cmd, "%s -c \"%s\"",gunzipl, arch->OpenFile);
  return cmd;
}
