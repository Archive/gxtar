/* gxTar - Gnomified Archive Frontend
 * Copyright (C) 1998 Chris Rogers
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option) 
 * any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  
 * 02111-1307, USA.
 */


#ifndef __ARJ_H__
#define __ARJ_H__

#include "archive.h"
#include "archivetypes.h"
#include <string.h>
#include "error.h"
#include "statusbar.h"

/*define all of the ARJ functions */
int arj_read(gchar *fle);
int arj_extract(gchar *dir);
int arj_add(GtkFileSelection *fs);
int arj_delete();
gchar * arj_view(gchar * fle);


#endif /* __ARJ_H__ */
