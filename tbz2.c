/* gxTar - Gnomified Archive Frontend
 * Copyright (C) 1998 Chris Rogers
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option) 
 * any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  
 * 02111-1307, USA.
 */

#include "tar.h"
#include "tbz2.h"


int tbz2_read(gchar * fle) {

  gchar *tmpOut;
  gchar *cmd;
  int result;

  tmpOut = tempnam("","gxtar");
  cmd = g_malloc(sizeof(gchar)*(10+strlen(bunzip2l)+strlen(fle)+strlen(tmpOut)));
  sprintf(cmd, "%s -c \"%s\" > %s",bunzip2l, fle, tmpOut);
  result = system(cmd); 
  result = tar_read(tmpOut);
  unlink(tmpOut);
  g_free(cmd);
  return result;
}


int  tbz2_extract(gchar * dir){

  GList *selection;
  gint index;
  gchar *text;
  gchar *tmpOut;
  gchar *cmd;
  gint result;
  FILE * src;
  

  selection = GTK_CLIST(arch->listFiles)->selection;
  tmpOut = NULL;
  
  if (selection == NULL) {
    cmd = g_malloc(sizeof(gchar)*(27+strlen(bunzip2l)+strlen(dir)+strlen(arch->OpenFile)));
    sprintf(cmd, "cd \"%s\"; %s -c \"%s\" | tar -xf - ", dir, bunzip2l, arch->OpenFile);
  }else{
    tmpOut = tempnam("","gxtar");
    src = fopen(tmpOut,"w");
    
    while(selection) {
      index = (gint) selection->data;
      gtk_clist_get_text(GTK_CLIST(arch->listFiles), index, COLNUM-1, &text);
      fputs(text,src); 
      gtk_clist_get_text(GTK_CLIST(arch->listFiles), index, 0, &text);
      fputs(text,src); 
      fputc('\n',src);

      selection= selection->next;
    }
    fclose(src);
    cmd = g_malloc(sizeof(gchar)*(31+strlen(bunzip2l)+strlen(dir)+strlen(arch->OpenFile)+strlen(tmpOut)));
    sprintf(cmd, "cd \"%s\"; %s -c \"%s\" | tar -xf - -T %s", dir, bunzip2l, arch->OpenFile, tmpOut);
  }
  
  result = system(cmd); 
  if (tmpOut != NULL) unlink(tmpOut);  
  g_free(cmd);
  return result;
}

int tbz2_add(GtkFileSelection *fs){

  /*ok, since we can't directly add to zipped tars, lets pretend*/
  gchar *tmpOut;
  gchar *cmd;
  int result;

  tmpOut = tempnam("","gxtar");
  
  if (!arch->Blank) {
    cmd = g_malloc(sizeof(gchar)*(10+strlen(bunzip2l)+strlen(arch->OpenFile)+strlen(tmpOut)));
    sprintf(cmd, "%s -c \"%s\" > %s",bunzip2l, arch->OpenFile, tmpOut);
    result = system(cmd); 
    g_free(cmd);
  }
  
  tar_add_actual(tmpOut,fs);
  
  cmd = g_malloc(sizeof(gchar)*(strlen(bzip2l)+strlen(tmpOut)));
  sprintf(cmd, "%s %s",bzip2l,tmpOut);
  result = system(cmd); 
  g_free(cmd);

  cmd = g_malloc(sizeof(gchar)*(11+strlen(tmpOut)+strlen(arch->OpenFile) ));
  sprintf(cmd, "mv %s.bz2 \"%s\"",tmpOut, arch->OpenFile);
  result = system(cmd); 
  g_free(cmd);

  unlink(tmpOut);

  cmd = g_malloc(sizeof(gchar)*(6+strlen(tmpOut)));
  sprintf(cmd,"%s.bz2",tmpOut);
  unlink(cmd);
  g_free(cmd);
  g_free(tmpOut);

  return 0;
}


int tbz2_delete(gchar *fle){

 /*ok, since we can't directly delete from  zipped tars, lets pretend*/
  gchar *tmpOut;
  gchar *cmd;
  int result;
  gint nullnum;


  if (debug_off) nullnum = 13; else nullnum = 0;

  tmpOut = tempnam("","gxtar");
  cmd = g_malloc(sizeof(gchar)*(10+strlen(bunzip2l)+strlen(tmpOut)+strlen(arch->OpenFile) ));
  sprintf(cmd, "%s -c \"%s\" > %s",bunzip2l, arch->OpenFile, tmpOut);
  result = system(cmd); 
  g_free(cmd);

  result = tar_delete_actual(tmpOut,fle);

  cmd = g_malloc(sizeof(gchar)*(strlen(bzip2l)+strlen(tmpOut)+nullnum));
  sprintf(cmd, "%s %s",bzip2l, tmpOut);
  if (debug_off) strcat(cmd, " >> /dev/null");
  result = system(cmd); 
  g_free(cmd);

  cmd = g_malloc(sizeof(gchar)*(14+strlen(tmpOut)+strlen(arch->OpenFile)+nullnum));
  sprintf(cmd, "mv -f %s.bz2 \"%s\"",tmpOut, arch->OpenFile);
  if (debug_off) strcat(cmd, " >> /dev/null");
  result = system(cmd); 
  g_free(cmd);
  
  unlink(tmpOut); 

  cmd = g_malloc(sizeof(gchar)*(5+strlen(tmpOut)+nullnum));
  sprintf(cmd,"%s.bz2",tmpOut);  
  unlink(cmd);
  g_free(cmd);
  g_free(tmpOut);

  return 0;
}

gchar * tbz2_view(gchar *fle){
  gchar * cmd;
  cmd = g_malloc(sizeof(gchar)*(23+strlen(bunzip2l)+strlen(fle)+strlen(arch->OpenFile)));
  sprintf(cmd, "%s -c \"%s\" | tar -xOf - \"%s\"",bunzip2l, arch->OpenFile, fle);
  return cmd;
}
