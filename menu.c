/* gxTar - Gnomified Archive Frontend
 * Copyright (C) 1998 Chris Rogers
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option) 
 * any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  
 * 02111-1307, USA.
 */

#include "menu.h"
#include "properties.h"
#include <string.h>


/* The menu definitions: File/Exit and Help/About are mandatory */

void file_quit_callback (GtkWidget *widget, gpointer data){quitArchive();}
void file_new_callback(GtkWidget *widget, gpointer data){newArchive();}
void file_open_callback(GtkWidget *widget, gpointer data){openfileArchive();}
void file_close_callback(GtkWidget *widget, gpointer data){closeArchive();}
void actions_add_callback(GtkWidget *widget, gpointer data){ addArchive();}
void actions_delete_callback(GtkWidget *widget, gpointer data){deleteArchive();}
void actions_extract_callback(GtkWidget *widget, gpointer data){extractArchive();}
void actions_view_callback(GtkWidget *widget, gpointer data){viewFile();}
void actions_select_callback(GtkWidget *widget, gpointer data){selectAll();}
void actions_deselect_callback(GtkWidget *widget, gpointer data){deselectAll();}
void options_configuration_callback(GtkWidget *widget, gpointer data){show_properties_dialog(); }

void help_about_callback (GtkWidget *widget, void *data) {
  GtkWidget *about;
  const gchar *authors[] = {
    "Chris Rogers",
    NULL
  };
  
  about = gnome_about_new ( _("gxTar"),
			    VERSION,
			    /* copyrigth notice */
			    "(C) Chris Rogers 1998, 1999 ",
			    authors,
			    /* another comments */
			    _("Gnome Archive Program"
			      ),
			    NULL);
  gtk_widget_show (about);
  
  return;
}


GtkMenuFactory *create_menu ();

static GnomeUIInfo file_menu[]= {
 { 
    GNOME_APP_UI_ITEM,
    N_("_New Archive"), N_("New Archive"),
    file_new_callback, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_NEW,
    'N', GDK_CONTROL_MASK, NULL
  },

  { 
    GNOME_APP_UI_ITEM,
    N_("_Open Archive"), N_("Open Archive"),
    file_open_callback, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK,GNOME_STOCK_MENU_OPEN,
    'O', GDK_CONTROL_MASK, NULL
  },
 { 
    GNOME_APP_UI_ITEM,
    N_("_Close Archive"), N_("Close Archive"),
    file_close_callback, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK,GNOME_STOCK_MENU_CLOSE,
    0, GDK_CONTROL_MASK, NULL
  },

 GNOMEUIINFO_SEPARATOR,
 { 
    GNOME_APP_UI_ITEM,
    N_("E_xit"), N_("Exit gnomba"),
    file_quit_callback, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_QUIT,
    'Q', GDK_CONTROL_MASK, NULL
  },
  GNOMEUIINFO_END
};


static GnomeUIInfo action_menu[]=
{
  {
    GNOME_APP_UI_ITEM,
    N_("_Add"), N_("Add item to archive"),
    actions_add_callback, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_COPY,
    0, (GdkModifierType)0, NULL
  }, 
  {
    GNOME_APP_UI_ITEM,
    N_("D_elete"), N_("Delete item from archive"),
    actions_delete_callback, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK,  GNOME_STOCK_MENU_CUT,
    0, (GdkModifierType)0, NULL
  }, 
 {
    GNOME_APP_UI_ITEM,
    N_("_Extract"), N_("Extract item from archive"),
    actions_extract_callback, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK,  GNOME_STOCK_MENU_CONVERT,
    0, (GdkModifierType)0, NULL
  }, 
 {
    GNOME_APP_UI_ITEM,
    N_("_View"), N_("View item"),
    actions_view_callback, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK,  GNOME_STOCK_MENU_SEARCH,
    0, (GdkModifierType)0, NULL
  }, 
  GNOMEUIINFO_SEPARATOR,
  {
    GNOME_APP_UI_ITEM,
    N_("_Select All"), N_("Select allitems"),
    actions_select_callback, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL,
    'A', GDK_CONTROL_MASK, NULL
  }, 
 {
    GNOME_APP_UI_ITEM,
    N_("_Deselect All"), N_("DeSelect all items"),
    actions_deselect_callback, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL,
    'D', GDK_CONTROL_MASK, NULL
  }, 
  GNOMEUIINFO_END
};

static GnomeUIInfo options_menu[]=
{
  {
    GNOME_APP_UI_ITEM,
    N_("_Preferences"), N_("Application Preferences"),
    options_configuration_callback, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_PREF,
    0, (GdkModifierType)0, NULL
  }, 
  GNOMEUIINFO_END
};



static GnomeUIInfo help_menu[]=
{
  {
    GNOME_APP_UI_ITEM,
    N_("_About..."), N_("Info about Gnomba"),
    help_about_callback, NULL, NULL,
    GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_ABOUT,
    0, (GdkModifierType)0, NULL
  }, 
  GNOMEUIINFO_END
};

static GnomeUIInfo main_menu[]= 
{
  GNOMEUIINFO_SUBTREE(N_("_File"), file_menu),
  GNOMEUIINFO_SUBTREE(N_("_Action"), action_menu),
  GNOMEUIINFO_SUBTREE(N_("_Options"), options_menu),
  GNOMEUIINFO_SUBTREE(N_("_Help"), help_menu),
  GNOMEUIINFO_END
};


void createMenu(GtkWidget *frame)
     
{
  gnome_app_create_menus (GNOME_APP (frame), main_menu);
}





