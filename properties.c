
#include "properties.h"

/*
  -program locations
  -program paramaters??
  -prompt on other feature maybe?
  -mime types
*/

static GtkWidget *propbox = NULL;
int TRememberPosition;
int TPromptOnDelete;
int TPromptOnQuit;
gint Tdebug_off;
GtkWidget *wft, *wlw, *wlh, *tarlocation, *gziplocation, *gunziplocation, *ziplocation, *bzip2location, *bunzip2location, *unarjlocation;

typedef struct _browseinfo {
  GtkWidget * text;
  GtkWidget * window;
} browseinfo;

static void rememberpos_cb( GtkWidget *widget, gpointer data)
{
  TRememberPosition = GTK_TOGGLE_BUTTON(widget)->active;
  gnome_property_box_changed(GNOME_PROPERTY_BOX(propbox));
}

static void promptondelete_cb( GtkWidget *widget, gpointer data)
{
  TPromptOnDelete = GTK_TOGGLE_BUTTON(widget)->active;
  gnome_property_box_changed(GNOME_PROPERTY_BOX(propbox));
}

static void promptonquit_cb( GtkWidget *widget, gpointer data)
{
  TPromptOnQuit = GTK_TOGGLE_BUTTON(widget)->active;
  gnome_property_box_changed(GNOME_PROPERTY_BOX(propbox));
}

static void tapedevice_cb( GtkWidget *widget, gpointer data)
{
  gnome_property_box_changed(GNOME_PROPERTY_BOX(propbox));
}

static void lessheight_cb( GtkWidget *widget, gpointer data)
{
  gnome_property_box_changed(GNOME_PROPERTY_BOX(propbox));
}

static void lesswidth_cb( GtkWidget *widget, gpointer data)
{
  gnome_property_box_changed(GNOME_PROPERTY_BOX(propbox));
}


static void apply_cb( GtkWidget   *w, gint pagenum, gpointer data)
{
  if ((pagenum == 0)||(pagenum == -1)) {
    
    RememberPosition = TRememberPosition;
    PromptOnDelete = TPromptOnDelete;
    PromptOnQuit = TPromptOnQuit;
    debug_off = Tdebug_off;
    
    if (ForcedTar != NULL) g_free(ForcedTar);
    ForcedTar = g_malloc(strlen(gtk_entry_get_text(GTK_ENTRY(wft)))+1);
    strcpy(ForcedTar, gtk_entry_get_text(GTK_ENTRY(wft)));

   

    less_w = atoi(gtk_entry_get_text(GTK_ENTRY(wlw)));
    less_h = atoi(gtk_entry_get_text(GTK_ENTRY(wlh)));

    /* now write these values back to the config */
    gnome_config_push_prefix ("gxtar/");
    gnome_config_set_int("Options/RememberPosition",RememberPosition);
    gnome_config_set_int("Options/PromptOnDelete", PromptOnDelete);
    gnome_config_set_int("Options/PromptOnQuit",  PromptOnQuit);
    gnome_config_set_int("Options/DebugOff", debug_off);
    gnome_config_set_int("Options/Less_W", less_w);
    gnome_config_set_int("Options/Less_H", less_h);
    gnome_config_set_string("Options/ForcedTar", ForcedTar);
    
    gnome_config_sync();
    gnome_config_pop_prefix();
  }

 if ((pagenum == 1)||(pagenum == -1)) {
 /*program locations*/
    if (tarl != NULL) g_free(tarl);
    tarl = g_malloc(strlen(gtk_entry_get_text(GTK_ENTRY(tarlocation)))+1);
    strcpy(tarl, gtk_entry_get_text(GTK_ENTRY(tarlocation)));

    if (gzipl != NULL) g_free(gzipl);
    gzipl = g_malloc(strlen(gtk_entry_get_text(GTK_ENTRY(gziplocation)))+1);
    strcpy(gzipl, gtk_entry_get_text(GTK_ENTRY(gziplocation)));

    if (gunzipl != NULL) g_free(gunzipl);
    gunzipl = g_malloc(strlen(gtk_entry_get_text(GTK_ENTRY(gunziplocation)))+1);
    strcpy(gunzipl, gtk_entry_get_text(GTK_ENTRY(gunziplocation)));

    if (zipl != NULL) g_free(zipl);
    zipl = g_malloc(strlen(gtk_entry_get_text(GTK_ENTRY(ziplocation)))+1);
    strcpy(zipl, gtk_entry_get_text(GTK_ENTRY(ziplocation)));

    if (bzip2l != NULL) g_free(bzip2l);
    bzip2l = g_malloc(strlen(gtk_entry_get_text(GTK_ENTRY(bzip2location)))+1);
    strcpy(bzip2l, gtk_entry_get_text(GTK_ENTRY(bzip2location)));

    if (bunzip2l != NULL) g_free(bunzip2l);
    bunzip2l = g_malloc(strlen(gtk_entry_get_text(GTK_ENTRY(bunzip2location)))+1);
    strcpy(bunzip2l, gtk_entry_get_text(GTK_ENTRY(bunzip2location)));
  
    if (unarjl != NULL) g_free(unarjl);
    unarjl = g_malloc(strlen(gtk_entry_get_text(GTK_ENTRY(unarjlocation)))+1);
    strcpy(unarjl, gtk_entry_get_text(GTK_ENTRY(unarjlocation)));
    
    gnome_config_push_prefix ("gxtar/");

    gnome_config_set_string("Options/TarLocation", tarl);
    gnome_config_set_string("Options/GzipLocation", gzipl);
    gnome_config_set_string("Options/GunzipLocation", gunzipl);
    gnome_config_set_string("Options/ZipLocation", zipl);
    gnome_config_set_string("Options/Bzip2Location", bzip2l);
    gnome_config_set_string("Options/Bunzip2Location", bunzip2l);
    gnome_config_set_string("Options/UnarjLocation", unarjl);

    gnome_config_sync();
    gnome_config_pop_prefix();
}

}

static void destroy_cb( GtkWidget   *w, gpointer data)
{
  gtk_widget_destroy (propbox);
  propbox = NULL;
}

void browse_select( GtkWidget *w, browseinfo *data) {

  gtk_entry_set_text(GTK_ENTRY(data->text), 
		     gtk_file_selection_get_filename (GTK_FILE_SELECTION(data->window)));
  gtk_object_destroy (GTK_OBJECT (data->window));
  g_free(data);
  gnome_property_box_changed(GNOME_PROPERTY_BOX(propbox));
}

void browse_cb( GtkWidget *w, gpointer data) {
  GtkWidget *window;
  browseinfo * bi;

  window = gtk_file_selection_new (_("Select Program Location"));
  gtk_file_selection_hide_fileop_buttons (GTK_FILE_SELECTION (window));
  gtk_window_position (GTK_WINDOW (window), GTK_WIN_POS_MOUSE);
  gtk_signal_connect (GTK_OBJECT (window), "destroy",
		      GTK_SIGNAL_FUNC(gtk_widget_destroyed),
		      &window);

  bi = g_malloc(sizeof(browseinfo));
  bi->text = (GtkWidget *) data;
  bi->window = window;

  gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (window)->ok_button),
		      "clicked", GTK_SIGNAL_FUNC(browse_select),(gpointer) bi);
  gtk_signal_connect_object (GTK_OBJECT (GTK_FILE_SELECTION (window)->cancel_button),
			     "clicked", GTK_SIGNAL_FUNC(gtk_widget_destroy),
			     GTK_OBJECT (window));
  
  if (gtk_entry_get_text(GTK_ENTRY(data)) != NULL) gtk_file_selection_set_filename (GTK_FILE_SELECTION(window), gtk_entry_get_text(GTK_ENTRY(data)));
  
  gtk_widget_show (window);
  gtk_grab_add(window); 
  
  
}


void show_properties_dialog() {
  GtkWidget *cpage;
  GtkWidget *hbox, *vbox, *vbox2, *vbox3;
  GtkWidget *chkbox;
  GtkWidget *label;
  GtkWidget *textbox;
  GtkWidget *cmdbrowse;
  gchar tlw[10];
  gchar tlh[10]; 

  if(propbox) return;

  /* Copy current setting into temporary */
  TRememberPosition = RememberPosition;
  TPromptOnDelete = PromptOnDelete;
  TPromptOnQuit = PromptOnQuit;
  Tdebug_off = debug_off;
  snprintf(tlw, 9, "%d",less_w);
  snprintf(tlh, 9, "%d",less_h);

  propbox = gnome_property_box_new();
  gtk_window_set_modal(GTK_WINDOW(propbox), TRUE);
  gtk_window_set_title(GTK_WINDOW(&GNOME_PROPERTY_BOX(propbox)->dialog.window), _("gxTar Preferences"));
  
  cpage = gtk_vbox_new(FALSE, 0);
  gtk_container_set_border_width(GTK_CONTAINER(cpage), GNOME_PAD_SMALL);
  
  hbox = gtk_hbox_new(TRUE, 0);
  gtk_box_pack_start(GTK_BOX(cpage), hbox, FALSE, TRUE, GNOME_PAD_SMALL);
  chkbox = gtk_check_button_new_with_label(_("Remember Position"));
  GTK_TOGGLE_BUTTON(chkbox)->active = TRememberPosition;
  gtk_signal_connect(GTK_OBJECT(chkbox), "clicked",
		     (GtkSignalFunc)rememberpos_cb, NULL);
  gtk_box_pack_start(GTK_BOX(hbox), chkbox, TRUE, TRUE, GNOME_PAD_SMALL);
  gtk_widget_show (chkbox);
  gtk_widget_show(hbox);

  hbox = gtk_hbox_new(TRUE, 0);
  gtk_box_pack_start(GTK_BOX(cpage), hbox, FALSE, TRUE, GNOME_PAD_SMALL);
  chkbox = gtk_check_button_new_with_label(_("Prompt On Delete"));
  GTK_TOGGLE_BUTTON(chkbox)->active = TPromptOnDelete;
  gtk_signal_connect(GTK_OBJECT(chkbox), "clicked",
		     (GtkSignalFunc)promptondelete_cb, NULL);
  gtk_box_pack_start(GTK_BOX(hbox), chkbox, TRUE, TRUE, GNOME_PAD_SMALL);
  gtk_widget_show (chkbox);
  gtk_widget_show(hbox);
  
  hbox = gtk_hbox_new(TRUE, 0);
  gtk_box_pack_start(GTK_BOX(cpage), hbox, FALSE, TRUE, GNOME_PAD_SMALL);
  chkbox = gtk_check_button_new_with_label(_("Prompt On Quit"));
  GTK_TOGGLE_BUTTON(chkbox)->active = TPromptOnQuit;
  gtk_signal_connect(GTK_OBJECT(chkbox), "clicked",
		     (GtkSignalFunc)promptonquit_cb, NULL);
  gtk_box_pack_start(GTK_BOX(hbox), chkbox, TRUE, TRUE, GNOME_PAD_SMALL);
  gtk_widget_show (chkbox);
  gtk_widget_show(hbox);

  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(cpage), hbox, FALSE, FALSE, GNOME_PAD_SMALL);
  label = gtk_label_new("Tape Device:");
  textbox = gtk_entry_new();
  wft = textbox;
  if (ForcedTar != NULL)
    gtk_entry_set_text(GTK_ENTRY(textbox),  ForcedTar);

  gtk_signal_connect(GTK_OBJECT(textbox), "key_press_event",
		     (GtkSignalFunc) tapedevice_cb, NULL);

  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, GNOME_PAD_SMALL);
  gtk_box_pack_start(GTK_BOX(hbox), textbox, FALSE, FALSE, GNOME_PAD_SMALL);
  gtk_widget_show (textbox);
  gtk_widget_show (label);
  gtk_widget_show(hbox);

  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(cpage), hbox, FALSE, FALSE, GNOME_PAD_SMALL);
  label = gtk_label_new("Default Viewer Width:");
  textbox = gtk_entry_new();
  wlw = textbox;
  gtk_entry_set_text(GTK_ENTRY(textbox),  tlw);
  gtk_signal_connect(GTK_OBJECT(textbox), "key_press_event",
		     (GtkSignalFunc) lesswidth_cb, NULL);

  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, GNOME_PAD_SMALL);
  gtk_box_pack_start(GTK_BOX(hbox), textbox, FALSE, FALSE, GNOME_PAD_SMALL);
  gtk_widget_show (textbox);
  gtk_widget_show (label);
  gtk_widget_show(hbox);

  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(cpage), hbox, FALSE, FALSE, GNOME_PAD_SMALL);
  label = gtk_label_new("Default Viewer Height:");
  textbox = gtk_entry_new();
  wlh = textbox;
  gtk_entry_set_text(GTK_ENTRY(textbox),  tlh);
  gtk_signal_connect(GTK_OBJECT(textbox), "key_press_event",
		     (GtkSignalFunc) lessheight_cb, NULL);

  gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, GNOME_PAD_SMALL);
  gtk_box_pack_start(GTK_BOX(hbox), textbox, FALSE, FALSE, GNOME_PAD_SMALL);
  
  gtk_widget_show (textbox);
  gtk_widget_show (label);
  gtk_widget_show(hbox);

  gtk_widget_show(cpage);
  label = gtk_label_new(_("Configuration"));
  gtk_notebook_append_page(GTK_NOTEBOOK(GNOME_PROPERTY_BOX(propbox)->notebook), 
			   cpage, label);
  


  /* Pogram Locations */

  cpage = gtk_vbox_new(FALSE, 0);
  gtk_container_set_border_width(GTK_CONTAINER(cpage), GNOME_PAD_SMALL);

  hbox = gtk_hbox_new(FALSE,0);
  vbox = gtk_vbox_new(FALSE, 0);
  vbox2 = gtk_vbox_new(FALSE, 0);
  vbox3 = gtk_vbox_new(FALSE, 0);

  gtk_box_pack_start(GTK_BOX(cpage), hbox, FALSE, FALSE, GNOME_PAD_SMALL);
  gtk_box_pack_start(GTK_BOX(hbox), vbox, FALSE, FALSE, GNOME_PAD_SMALL);
  gtk_box_pack_start(GTK_BOX(hbox), vbox2, TRUE, TRUE, GNOME_PAD_SMALL);
  gtk_box_pack_start(GTK_BOX(hbox), vbox3, FALSE, FALSE, GNOME_PAD_SMALL);

  label = gtk_label_new("tar:");
  textbox = gtk_entry_new();
  tarlocation = textbox;
  gtk_entry_set_text(GTK_ENTRY(textbox),  tarl);
  gtk_signal_connect(GTK_OBJECT(textbox), "key_press_event",
		     (GtkSignalFunc) lessheight_cb, NULL);
  cmdbrowse = gtk_button_new_with_label("...");
  gtk_signal_connect(GTK_OBJECT(cmdbrowse), "clicked",
		     (GtkSignalFunc) browse_cb, textbox);
  gtk_box_pack_start(GTK_BOX(vbox), label, TRUE, FALSE, GNOME_PAD_SMALL);
  gtk_box_pack_start(GTK_BOX(vbox2), textbox, FALSE, FALSE, GNOME_PAD_SMALL);
  gtk_box_pack_start(GTK_BOX(vbox3), cmdbrowse, TRUE, FALSE, GNOME_PAD_SMALL);
  gtk_widget_show(textbox);
  gtk_widget_show(label);
  gtk_widget_show(cmdbrowse);

  label = gtk_label_new("gzip:");
  textbox = gtk_entry_new();
  gziplocation = textbox;
  gtk_entry_set_text(GTK_ENTRY(textbox),  gzipl);
  gtk_signal_connect(GTK_OBJECT(textbox), "key_press_event",
		     (GtkSignalFunc) lessheight_cb, NULL);
  cmdbrowse = gtk_button_new_with_label("...");
  gtk_signal_connect(GTK_OBJECT(cmdbrowse), "clicked",
		     (GtkSignalFunc) browse_cb, textbox);
  gtk_box_pack_start(GTK_BOX(vbox), label, TRUE, FALSE, GNOME_PAD_SMALL);
  gtk_box_pack_start(GTK_BOX(vbox2), textbox, FALSE, FALSE, GNOME_PAD_SMALL);
  gtk_box_pack_start(GTK_BOX(vbox3), cmdbrowse, TRUE, FALSE, GNOME_PAD_SMALL);
  gtk_widget_show (textbox);
  gtk_widget_show (label);
  gtk_widget_show(cmdbrowse);
  
  label = gtk_label_new("gunzip:");
  textbox = gtk_entry_new();
  gunziplocation = textbox;
  gtk_entry_set_text(GTK_ENTRY(textbox),  gunzipl);
  gtk_signal_connect(GTK_OBJECT(textbox), "key_press_event",
		     (GtkSignalFunc) lessheight_cb, NULL);
  cmdbrowse = gtk_button_new_with_label("...");
  gtk_signal_connect(GTK_OBJECT(cmdbrowse), "clicked",
		     (GtkSignalFunc) browse_cb, textbox);
  
  gtk_box_pack_start(GTK_BOX(vbox), label, TRUE, FALSE, GNOME_PAD_SMALL);
  gtk_box_pack_start(GTK_BOX(vbox2), textbox, FALSE, FALSE, GNOME_PAD_SMALL);
  gtk_box_pack_start(GTK_BOX(vbox3), cmdbrowse, TRUE, FALSE, GNOME_PAD_SMALL);
  gtk_widget_show (textbox);
  gtk_widget_show (label);
  gtk_widget_show(cmdbrowse);

  label = gtk_label_new("zip:");
  textbox = gtk_entry_new();
  ziplocation = textbox;
  gtk_entry_set_text(GTK_ENTRY(textbox),  zipl);
  gtk_signal_connect(GTK_OBJECT(textbox), "key_press_event",
		     (GtkSignalFunc) lessheight_cb, NULL);
  cmdbrowse = gtk_button_new_with_label("...");
  gtk_signal_connect(GTK_OBJECT(cmdbrowse), "clicked",
		     (GtkSignalFunc) browse_cb, textbox);
  
  gtk_box_pack_start(GTK_BOX(vbox), label, TRUE, FALSE, GNOME_PAD_SMALL);
  gtk_box_pack_start(GTK_BOX(vbox2), textbox, FALSE, FALSE, GNOME_PAD_SMALL);
  gtk_box_pack_start(GTK_BOX(vbox3), cmdbrowse, TRUE, FALSE, GNOME_PAD_SMALL);
  gtk_widget_show (textbox);
  gtk_widget_show (label);
  gtk_widget_show(cmdbrowse);

  label = gtk_label_new("bzip2:");
  textbox = gtk_entry_new();
  bzip2location = textbox;
  gtk_entry_set_text(GTK_ENTRY(textbox),  bzip2l);
  gtk_signal_connect(GTK_OBJECT(textbox), "key_press_event",
		     (GtkSignalFunc) lessheight_cb, NULL);
  cmdbrowse = gtk_button_new_with_label("...");
  gtk_signal_connect(GTK_OBJECT(cmdbrowse), "clicked",
		     (GtkSignalFunc) browse_cb, textbox);
  
  gtk_box_pack_start(GTK_BOX(vbox), label, TRUE, FALSE, GNOME_PAD_SMALL);
  gtk_box_pack_start(GTK_BOX(vbox2), textbox, FALSE, FALSE, GNOME_PAD_SMALL);
  gtk_box_pack_start(GTK_BOX(vbox3), cmdbrowse, TRUE, FALSE, GNOME_PAD_SMALL);
  gtk_widget_show (textbox);
  gtk_widget_show (label);
  gtk_widget_show(cmdbrowse);

  label = gtk_label_new("buzip2:");
  textbox = gtk_entry_new();
  bunzip2location = textbox;
  gtk_entry_set_text(GTK_ENTRY(textbox),  bunzip2l);
  gtk_signal_connect(GTK_OBJECT(textbox), "key_press_event",
		     (GtkSignalFunc) lessheight_cb, NULL);
  cmdbrowse = gtk_button_new_with_label("...");
  gtk_signal_connect(GTK_OBJECT(cmdbrowse), "clicked",
		     (GtkSignalFunc) browse_cb, textbox);
  
  gtk_box_pack_start(GTK_BOX(vbox), label, TRUE, FALSE, GNOME_PAD_SMALL);
  gtk_box_pack_start(GTK_BOX(vbox2), textbox, FALSE, FALSE, GNOME_PAD_SMALL);
  gtk_box_pack_start(GTK_BOX(vbox3), cmdbrowse, TRUE, FALSE, GNOME_PAD_SMALL);
  gtk_widget_show (textbox);
  gtk_widget_show (label);
  gtk_widget_show(cmdbrowse);

  label = gtk_label_new("unarj:");
  textbox = gtk_entry_new();
  unarjlocation = textbox;
  gtk_entry_set_text(GTK_ENTRY(textbox),  unarjl);
  gtk_signal_connect(GTK_OBJECT(textbox), "key_press_event",
		     (GtkSignalFunc) lessheight_cb, NULL);
  cmdbrowse = gtk_button_new_with_label("...");
  gtk_signal_connect(GTK_OBJECT(cmdbrowse), "clicked",
		     (GtkSignalFunc) browse_cb, textbox);
  
 
  gtk_box_pack_start(GTK_BOX(vbox), label, TRUE, FALSE, GNOME_PAD_SMALL);
  gtk_box_pack_start(GTK_BOX(vbox2), textbox, FALSE, FALSE, GNOME_PAD_SMALL);
  gtk_box_pack_start(GTK_BOX(vbox3), cmdbrowse, TRUE, FALSE, GNOME_PAD_SMALL);
  gtk_widget_show (textbox);
  gtk_widget_show (label);
  gtk_widget_show(cmdbrowse);

  gtk_widget_show(hbox);
  gtk_widget_show(vbox);
  gtk_widget_show(vbox2);
  gtk_widget_show(vbox3);
  gtk_widget_show(cpage);
  label = gtk_label_new(_("Program Locations"));
  gtk_notebook_append_page(GTK_NOTEBOOK(GNOME_PROPERTY_BOX(propbox)->notebook), 
			   cpage, label);

  gtk_signal_connect (GTK_OBJECT (propbox), "destroy",
		      GTK_SIGNAL_FUNC (destroy_cb), NULL);
  gtk_signal_connect (GTK_OBJECT (propbox), "apply",
		      GTK_SIGNAL_FUNC (apply_cb), NULL);
  gtk_widget_show(propbox);
}
