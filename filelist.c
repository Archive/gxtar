/* gxTar - Gnomified Archive Frontend
 * Copyright (C) 1998 Chris Rogers
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option) 
 * any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  
 * 02111-1307, USA.
 */

#include "archive.h"
#include "archivetypes.h"
#include "error.h"
#include "filelist.h"

enum {
  TARGET_URI_LIST,
};


void view_file(){viewFile();}
void delete_file(){deleteArchive();}
void extract_file(){extractArchive();}


static GnomeUIInfo popup_menu[]= 
{
  GNOMEUIINFO_ITEM_NONE(N_("View"),N_("View"),view_file),
  GNOMEUIINFO_ITEM_NONE(N_("Delete"),N_("Delete"), delete_file),
  GNOMEUIINFO_ITEM_NONE(N_("Extract"),N_("Extract"), extract_file),
  GNOMEUIINFO_END
};


static void clist_click_column (GtkCList *clist, gint column, gpointer data)
{

  if (column != clist->sort_column)
    gtk_clist_set_sort_column (clist, column);
  else {
    if (clist->sort_type == GTK_SORT_ASCENDING)
      clist->sort_type = GTK_SORT_DESCENDING;
    else
      clist->sort_type = GTK_SORT_ASCENDING;
  }
  gtk_clist_sort (clist);
}

static void clist_drop(GtkWidget *widget, GdkDragContext *context,
                       gint x, gint y, GtkSelectionData *selection_data,
                       guint info, guint time, gpointer data) {
  GList *names, *list;
  
  if(arch->OpenFile == NULL) {
    ShowError(_("There is no currently open archive."));
    return;
  }

  switch(info) {
  case TARGET_URI_LIST:
    list = names = gnome_uri_list_extract_filenames (selection_data->data);
    while(names) {
      archList[arch->filetype].addit((gchar *)names->data);
      names = names->next;
    }
    gnome_uri_list_free_strings(list);
    openArchive();
    break;
  default:
  }
}

void createFileList(GtkWidget * frame) {
  
  static GtkTargetEntry drop_types [] = {
    { "text/uri-list", 0, TARGET_URI_LIST}
  };
  static gint n_drop_types = sizeof (drop_types) / sizeof(drop_types[0]);

  GtkWidget *scrolled_window;
  gchar *listTitles[6];
  GtkWidget *popup;

  listTitles[0] = g_malloc(sizeof(gchar) *5 );listTitles[0] = "Name\0";
  listTitles[1] = g_malloc(sizeof(gchar) *5 );listTitles[1] = "Date\0";
  listTitles[2] = g_malloc(sizeof(gchar) *5 );listTitles[2] = "Size\0";
  listTitles[3] = g_malloc(sizeof(gchar) *12 );listTitles[3] ="Permissions\0";
  listTitles[4] = g_malloc(sizeof(gchar) *6 );listTitles[4] = "Ratio\0";
  listTitles[5] = g_malloc(sizeof(gchar) *5 );listTitles[5] = "Path\0";
    
  arch = g_malloc(sizeof(struct archive));
  arch->listFiles  =  gtk_clist_new_with_titles( 6, listTitles);

  gtk_clist_set_column_width( GTK_CLIST (  arch->listFiles),0,45);
  gtk_clist_set_column_width( GTK_CLIST (  arch->listFiles),1,45);
  gtk_clist_set_column_width( GTK_CLIST (  arch->listFiles),2,45);
  gtk_clist_set_column_width( GTK_CLIST (  arch->listFiles),3,65);
  gtk_clist_set_column_width( GTK_CLIST (  arch->listFiles),4,45);
  gtk_clist_set_column_width( GTK_CLIST (  arch->listFiles),5,70);
  gtk_clist_set_selection_mode(GTK_CLIST (  arch->listFiles), GTK_SELECTION_EXTENDED);
  gtk_clist_column_titles_active(GTK_CLIST (  arch->listFiles));
  
  gtk_drag_dest_set (arch->listFiles,
                     GTK_DEST_DEFAULT_MOTION |
                     GTK_DEST_DEFAULT_HIGHLIGHT |
                     GTK_DEST_DEFAULT_DROP,
                     drop_types, n_drop_types,
                     GDK_ACTION_COPY);
  
  gtk_signal_connect (GTK_OBJECT (arch->listFiles), "drag_data_received",
                      GTK_SIGNAL_FUNC(clist_drop), NULL);
  
  gtk_signal_connect (GTK_OBJECT (arch->listFiles), "click_column",
		      (GtkSignalFunc) clist_click_column, NULL);
  
  scrolled_window=gtk_scrolled_window_new(NULL, NULL);
  gtk_container_add(GTK_CONTAINER(scrolled_window), arch->listFiles);
  gtk_box_pack_start(GTK_BOX(frame), scrolled_window, TRUE, TRUE, 0);
  gtk_widget_show(scrolled_window);
  gtk_widget_show(arch->listFiles);


  popup = gnome_popup_menu_new(popup_menu);
  gnome_popup_menu_attach(popup,arch->listFiles,NULL);
}
