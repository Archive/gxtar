/* gxTar - Gnomified Archive Frontend
 * Copyright (C) 1998 Chris Rogers
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option) 
 * any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  
 * 02111-1307, USA.
 */

#include "archive.h"
#include "archivetypes.h"

/*this really is not cool, but I can't for the life of me get it to work with ** */
gchar *  splitNamePath(gchar * src, int tst){
  
  gchar * name, * path; 
  gchar * slash;
  int sz;
  
  slash = strrchr(src, '/');
  if (slash == src + strlen(src) -1) { 
    
    slash[0] = '\0';
    
    slash = strrchr(src, '/');
    
    if (slash == NULL) { 
      
      name = g_malloc( strlen(src) + 2); 
      strcpy(name, src);
      name[strlen(src)] = '/';
      name[strlen(src)+1] = '\0';
      
      path= g_malloc( 2 );
      strcpy(path," ");
      path[1] = '\0';
    }
    else {
      
      name = g_malloc( (int)  src + strlen(src) -  (int) slash + 1);
      strcpy(name, slash+1);
      name[(int) src + strlen(src) -  (int) slash - 1] = '/';
      name[(int) src + strlen(src) -  (int) slash ] = '\0';
      sz = strlen(src) - strlen(name)+1;
      path = g_malloc(sz);
      strncpy(path, src,sz-1);
      path[sz - 1] = '\0';
      
      
    }
  }
  else { 
    
    if (slash == NULL) {
      
      name = g_malloc(strlen(src)+1);
      strcpy(name, src);
      name[strlen(src)] = '\0';
      
      path = g_malloc( 1 ); /* 2 */
      /*      strcpy(path," ");*/
      path[0] = '\0'; /* 1 */
      
    }
    else{
      
      name = g_malloc( (int)  src + strlen(src) -  (int) slash);
      strcpy(name, slash+1);
      name[(int) src + strlen(src) -  (int) slash - 1] = '\0';
      
      sz = strlen(src) - strlen(name)+1;
      path = g_malloc(sz);
      strncpy(path, src,sz-1);
      path[sz - 1] = '\0';
      
    }
  }

  if (tst == 0) {g_free(path);return name;}
  else {g_free(name);return path; }
  
}
