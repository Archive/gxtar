/* gxTar - Gnomified Archive Frontend
 * Copyright (C) 1998 Chris Rogers
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option) 
 * any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  
 * 02111-1307, USA.
 */

#include "arj.h"




int arj_read(gchar * fle) {

  gchar *text[COLNUM];
  gchar tmp[7][1024];
  gchar trash[1024];
  FILE * fl;
  gchar *cmd;
  int i,j;

  cmd = g_malloc(sizeof(gchar)*(6+strlen(fle)+strlen(unarjl)));
  sprintf(cmd, "%s l \"%s\"", unarjl, fle);
 

  if ((fl = popen(cmd,"r")) == NULL) {
    return -1 ;
  }
  g_free(cmd);

  for  (i = 0; i < 6; i++) fgets(trash, 1023, fl);

  j = 0;
  while (!feof(fl))  
    {
      fscanf(fl,"%s %s %s %s %s %s",tmp[0],tmp[1],tmp[2],tmp[3],tmp[4],tmp[5]);

      text[0] = tmp[0];
      text[2] = tmp[1];
      text[4] = tmp[3];
      text[1] = tmp[4];
      text[3] = NULL;
      text[5] = NULL;
      
      fgets(tmp[6],1023,fl); /*workaround for spaces in last string*/


      gtk_clist_append( GTK_CLIST (arch->listFiles) , text);
      j++;
}
  gtk_clist_remove (GTK_CLIST (arch->listFiles),j-1); 
  gtk_clist_remove (GTK_CLIST (arch->listFiles),j-2); 
  gtk_clist_remove (GTK_CLIST (arch->listFiles),j-3); 
  pclose(fl);
  return 0; 
}


int  arj_extract(gchar * dir){

  gchar *cmd;
  int result;

  cmd = g_malloc(sizeof(gchar)*(13+strlen(dir)+strlen(arch->OpenFile)+strlen(unarjl)));

  if (GTK_CLIST(arch->listFiles)->selection == NULL) {
    sprintf(cmd, "cd \"%s\"; %s x \"%s\"", dir, unarjl, arch->OpenFile);
  }
  else{
    return -1;
  }
  if (debug_off) strcat(cmd, " >> /dev/null");
  result = system(cmd);
  g_free(cmd);
  return result;
}


int arj_add(GtkFileSelection *fs){
  
  return -1;
}


int arj_delete(gchar *fle){
  
  return -1;
}



gchar * arj_view(gchar *fle){

  return 0;
   
}

