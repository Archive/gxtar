/* gxTar - Gnomified Archive Frontend
 * Copyright (C) 1998 Chris Rogers
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option) 
 * any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  
 * 02111-1307, USA.
 */

#include <string.h>
#include "archive.h"
#include "error.h"
#include "statusbar.h"
#include "archivetypes.h"
#include "tar.h"
#include "zip.h"
#include "tgz.h"
#include "gz.h"
#include "tbz2.h"
#include "bz2.h"
#include "arj.h"

/* File Stuff */

struct archive_type archList[KNOWNEXT] =

{
  {"application/x-gunzip",    gz_read, gz_extract,  gz_add,  gz_delete,  gz_view},
  {"application/x-compressed-tar",  tgz_read, tgz_extract, tgz_add, tgz_delete, tgz_view},
  {"application/x-tar",  tar_read, tar_extract, tar_add, tar_delete, tar_view}, /* or x-gtar */
  {"application/zip",  zip_read, zip_extract, zip_add, zip_delete, zip_view},
  {"application/x-bzip",  bz2_read, bz2_extract, bz2_add, bz2_delete, bz2_view}, /*or x-bzip2*/
  {"application/x-bzip-compressed-tar", tbz2_read, tbz2_extract, tbz2_add,tbz2_delete, tbz2_view},
  {"application/x-arj",  arj_read, arj_extract, arj_add, arj_delete, arj_view},
};


/******    OPEN FILE     *******/

void file_selection_hide_fileops (GtkWidget *widget, GtkFileSelection *fs) {
  gtk_file_selection_hide_fileop_buttons (fs);
}

void file_selection_ok (GtkWidget *widget, GtkFileSelection *fs) {

  if (arch->OpenFile != NULL) {g_free(arch->OpenFile);  arch->OpenFile = NULL;}
  arch->OpenFile = g_malloc0(strlen(gtk_file_selection_get_filename (GTK_FILE_SELECTION (fs)))+1);
  strcpy(arch->OpenFile, gtk_file_selection_get_filename (GTK_FILE_SELECTION (fs)));
  gtk_object_destroy (GTK_OBJECT (fs));

  /*now get the path from Openfile and save it for later*/
  if (LastODir != NULL) g_free(LastODir);
  LastODir =   splitNamePath(arch->OpenFile,1);

  openArchive();
}

void create_file_selection () {
  static GtkWidget *window = NULL;
  GtkWidget *button;
  
  if (!window)
    {
      window = gtk_file_selection_new (_("Open Archive"));
      gtk_file_selection_hide_fileop_buttons (GTK_FILE_SELECTION (window));
      gtk_window_position (GTK_WINDOW (window), GTK_WIN_POS_MOUSE);
      gtk_signal_connect (GTK_OBJECT (window), "destroy",
                          GTK_SIGNAL_FUNC(gtk_widget_destroyed),
                          &window);
      gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (window)->ok_button),
                          "clicked", GTK_SIGNAL_FUNC(file_selection_ok),
                          window);
      gtk_signal_connect_object (GTK_OBJECT (GTK_FILE_SELECTION (window)->cancel_button),
                                 "clicked", GTK_SIGNAL_FUNC(gtk_widget_destroy),
                                 GTK_OBJECT (window));
      button = gtk_button_new_with_label (_("Hide Fileops"));
      gtk_signal_connect (GTK_OBJECT (button), "clicked",
                          (GtkSignalFunc) file_selection_hide_fileops, 
                          (gpointer) window);
      gtk_box_pack_start (GTK_BOX (GTK_FILE_SELECTION (window)->action_area), 
                          button, FALSE, FALSE, 0);
      gtk_widget_show (button);
      button = gtk_button_new_with_label (_("Show Fileops"));
      gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
                                 (GtkSignalFunc) gtk_file_selection_show_fileop_buttons, 
                                 (gpointer) window);
      gtk_box_pack_start (GTK_BOX (GTK_FILE_SELECTION (window)->action_area), 
                          button, FALSE, FALSE, 0);
      gtk_widget_show (button);

      if (LastODir != NULL) gtk_file_selection_set_filename (GTK_FILE_SELECTION(window), LastODir);

    }
  if (!GTK_WIDGET_VISIBLE (window))
    {
      gtk_widget_show (window);
      gtk_grab_add(window);
    }
  else
    {
      gtk_widget_destroy (window);
      gtk_grab_remove(window);
    }
}
 

/*******      EXTRACT FILE WINDOW   ************/

void extract_selection_hide_fileops (GtkWidget *widget, GtkFileSelection *fs) {
  gtk_file_selection_hide_fileop_buttons (fs);
}

void extract_selection_ok (GtkWidget *widget, GtkFileSelection *fs) {
  gchar *dir;
  GdkCursor *cursor;

  dir = g_malloc0(strlen(gtk_file_selection_get_filename (GTK_FILE_SELECTION (fs)))+1);
  strcpy(dir, gtk_file_selection_get_filename (GTK_FILE_SELECTION (fs)));
  gtk_widget_destroy (GTK_WIDGET (fs));

  if (LastEDir != NULL) g_free(LastEDir);
  LastEDir =   g_malloc0(strlen(dir)+1);
  strcpy(LastEDir,dir);

  setStatusbar(_("Extracting File..."));
  cursor = gdk_cursor_new (150);
  gdk_window_set_cursor (window->window, cursor);
  gdk_cursor_destroy (cursor);
  while (gtk_events_pending()) gtk_main_iteration();
 
  archList[arch->filetype].extractit(dir);
  g_free(dir);

  clearStatusbar();
  cursor = gdk_cursor_new (0);
  gdk_window_set_cursor (window->window, cursor);
  gdk_cursor_destroy (cursor);
}


void create_extract_selection () {
  static GtkWidget *window = NULL;
  GtkWidget *button;
  
  if (!window)
    {
      window = gtk_file_selection_new (_("Extract Archive to..."));
      gtk_file_selection_hide_fileop_buttons (GTK_FILE_SELECTION (window));
      gtk_window_position (GTK_WINDOW (window), GTK_WIN_POS_MOUSE);
      gtk_signal_connect (GTK_OBJECT (window), "destroy",
                          GTK_SIGNAL_FUNC(gtk_widget_destroyed),
                          &window);
      gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (window)->ok_button),
                          "clicked", GTK_SIGNAL_FUNC(extract_selection_ok),
                          window);
      gtk_signal_connect_object (GTK_OBJECT (GTK_FILE_SELECTION (window)->cancel_button),
                                 "clicked", GTK_SIGNAL_FUNC(gtk_widget_destroy),
                                 GTK_OBJECT (window));
      button = gtk_button_new_with_label (_("Hide Fileops"));
      gtk_signal_connect (GTK_OBJECT (button), "clicked",
                          (GtkSignalFunc) extract_selection_hide_fileops, 
                          (gpointer) window);
      gtk_box_pack_start (GTK_BOX (GTK_FILE_SELECTION (window)->action_area), 
                          button, FALSE, FALSE, 0);
      gtk_widget_show (button);
      button = gtk_button_new_with_label (_("Show Fileops"));
      gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
                                 (GtkSignalFunc) gtk_file_selection_show_fileop_buttons, 
                                 (gpointer) window);
      gtk_box_pack_start (GTK_BOX (GTK_FILE_SELECTION (window)->action_area), 
                          button, FALSE, FALSE, 0);
      gtk_widget_show (button);


      gtk_widget_hide(GTK_FILE_SELECTION(window)->file_list);
      gtk_widget_hide(GTK_FILE_SELECTION(window)->file_list->parent);

      if (LastEDir != NULL) gtk_file_selection_set_filename (GTK_FILE_SELECTION(window), LastEDir);

   }
  if (!GTK_WIDGET_VISIBLE (window))
    {
      gtk_widget_show (window);
      gtk_grab_add(window);
    }
  else
    {
      gtk_widget_destroy (window);
      gtk_grab_remove(window);
    }
}


/*******    ADD FILE WINDOW    ******/

void add_selection_hide_fileops (GtkWidget *widget, GtkFileSelection *fs) {
  gtk_file_selection_hide_fileop_buttons (fs);
}


/*This is all kinds of ugliness, _way_ too dependent on internal structs */
void add_selection_ok (GtkWidget *widget, GtkFileSelection *fs) {
  gchar * fle;
  GdkCursor * cursor;
 
  fle = g_malloc0(strlen(gtk_file_selection_get_filename (GTK_FILE_SELECTION (fs)))+1);
  strcpy(fle, gtk_file_selection_get_filename (GTK_FILE_SELECTION (fs)));
  
  if (LastODir != NULL) g_free(LastODir);
  LastODir =   splitNamePath(fle,1);
  
  setStatusbar(_("Adding File..."));
  cursor = gdk_cursor_new (150);
  gdk_window_set_cursor (window->window, cursor);
  gdk_cursor_destroy (cursor);
  while (gtk_events_pending()) gtk_main_iteration();
  
  archList[arch->filetype].addit(fs);
  openArchive(); /* this is still ugly too */
 
  gtk_widget_destroy (GTK_WIDGET (fs));
  g_free(fle);

}

void create_add_selection () {
  static GtkWidget *window = NULL;

  if (!window)
    {
      window = gtk_file_selection_new (_("Add file to archive"));
      gtk_file_selection_hide_fileop_buttons (GTK_FILE_SELECTION (window));
      gtk_window_position (GTK_WINDOW (window), GTK_WIN_POS_MOUSE);
      gtk_signal_connect (GTK_OBJECT (window), "destroy",
                          GTK_SIGNAL_FUNC(gtk_widget_destroyed),
                          &window);
      gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (window)->ok_button),
                          "clicked", GTK_SIGNAL_FUNC(add_selection_ok),
                          window);
      gtk_signal_connect_object (GTK_OBJECT (GTK_FILE_SELECTION (window)->cancel_button),
                                 "clicked", GTK_SIGNAL_FUNC(gtk_widget_destroy),
                                 GTK_OBJECT (window));

      gtk_widget_hide(GTK_FILE_SELECTION(window)->selection_entry);
      gtk_widget_hide(GTK_FILE_SELECTION(window)->selection_text);
      gtk_clist_set_selection_mode(GTK_CLIST (GTK_FILE_SELECTION(window)->file_list), GTK_SELECTION_EXTENDED);

      if (LastODir != NULL) gtk_file_selection_set_filename (GTK_FILE_SELECTION(window), LastODir);
    }

  if (!GTK_WIDGET_VISIBLE (window))
    {
      gtk_widget_show (window);
      gtk_grab_add(window);
    }
  else
    {
      gtk_widget_destroy (window);
      gtk_grab_remove(window);
    }
}


/****view files*****/

void closeView(GtkWidget *widget, gpointer *gp) {
  gtk_widget_destroy (GTK_WIDGET (gp));
}

void viewFile() {

  GList *selection;
  GtkWidget *app;
  GtkWidget *less;
  GtkWidget *okbutton;
  gchar *cmd;
  gchar *fle;
  GtkWidget *box, *box2;
  gchar *text;
  int index;
  GdkCursor *cursor;
  gchar *vtitle;
  gint sze;

  setStatusbar(_("Viewing file..."));
  cursor = gdk_cursor_new (150);
  gdk_window_set_cursor (window->window, cursor);
  gdk_cursor_destroy (cursor);
  while (gtk_events_pending()) gtk_main_iteration();

  /*we only view the first selected item*/
  selection = GTK_CLIST(arch->listFiles)->selection;
  
  if (selection == NULL) { 
    clearStatusbar();
    cursor = gdk_cursor_new (0);
    gdk_window_set_cursor (window->window, cursor);
    gdk_cursor_destroy (cursor);
    ShowError(_("No files selected!"));
    return;
  }

  index = (gint) selection->data;

  sze = 0;
  
  gtk_clist_get_text(GTK_CLIST(arch->listFiles), index, COLNUM-1, &text);
  sze = sze+ strlen(text);
  gtk_clist_get_text(GTK_CLIST(arch->listFiles), index, 0, &text);
  sze = sze + strlen(text);
  
  if (strchr(text,'/') != NULL){ /* if it is a drictory just quit */
    clearStatusbar();
    return;
  }
  fle = g_malloc(sizeof(gchar)*sze);
  fle[0] = '\0';
  gtk_clist_get_text(GTK_CLIST(arch->listFiles), index, COLNUM-1, &text);
  strcat(fle,text); 
  gtk_clist_get_text(GTK_CLIST(arch->listFiles), index, 0, &text);
  strcat(fle,text); 
  
  if ( (cmd = archList[arch->filetype].viewit(fle)) == 0) {
   
    cursor = gdk_cursor_new (0);
    gdk_window_set_cursor (window->window, cursor);
    gdk_cursor_destroy (cursor);
    clearStatusbar();
    ShowError(_("Error viewing file"));
    g_free(fle);
    return;
  }

  vtitle = g_malloc((strlen(fle)+7  ) *sizeof(gchar));
  strcpy(vtitle,_("View: "));
  strcat(vtitle,fle);

  app = gnome_app_new ("gxTar", vtitle);
  gtk_window_set_modal(GTK_WINDOW(app), TRUE);

  less = gnome_less_new();
  okbutton = gtk_button_new_with_label("Ok");
  box = gtk_vbox_new(FALSE, 0);
  
  gtk_box_pack_start(GTK_BOX(box), less, TRUE, TRUE, 0);
  box2 = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(box), box2, TRUE, TRUE, 0);
  gtk_box_pack_start(GTK_BOX(box2), okbutton, FALSE, FALSE, 0);
  gnome_app_set_contents(GNOME_APP(app), box);
  gnome_less_set_fixed_font(GNOME_LESS(less), TRUE);
  gtk_widget_set_usize (less, less_w, less_h);

  gtk_signal_connect (GTK_OBJECT (okbutton), "clicked",
		      GTK_SIGNAL_FUNC (closeView), app);

  gtk_widget_show(okbutton);
  gtk_widget_show(less);
  gtk_widget_show(app);
  gnome_less_show_command(GNOME_LESS(less),cmd);

  g_free(vtitle);
  g_free(fle);
  g_free(cmd);

  clearStatusbar();
  cursor = gdk_cursor_new (0);
  gdk_window_set_cursor (window->window, cursor);
  gdk_cursor_destroy (cursor);

}


/****************Open Archive*****************/
void openArchive() {
  
  gint i;
  gint found = -1;
  gchar * title;
  const gchar * mimetype;
  GdkCursor *cursor;

  /*make sure it is not just a directory */
  if (arch->OpenFile[strlen(arch->OpenFile)-1] == '/') {
    ShowError("You must specify the name of an archive to open.");
    if (arch->OpenFile) {g_free(arch->OpenFile); arch->OpenFile = NULL;}
    return;
  }

  setStatusbar(_("Opening File..."));
  cursor = gdk_cursor_new (150);
  gdk_window_set_cursor (window->window, cursor);
  gdk_cursor_destroy (cursor);
  while (gtk_events_pending()) gtk_main_iteration();

  mimetype = gnome_mime_type(arch->OpenFile);
  if (!debug_off) g_print("mime:%s\n",mimetype);

  /*check against our known mimetypes*/

  for (i = 0; i < KNOWNEXT; i++) 
    if (strcmp(mimetype, archList[i].mimetype)==0) found = i;
  
  /*check if it is our magic tar device*/
  if ((found ==-1) && (ForcedTar)) 
    if (strcmp(arch->OpenFile,ForcedTar)==0) found = TAR;  

  if (found ==-1) {
    if (arch->OpenFile != NULL) {g_free(arch->OpenFile); arch->OpenFile = NULL;}
    clearStatusbar();
    cursor = gdk_cursor_new (0);
    gdk_window_set_cursor (window->window, cursor);
    gdk_cursor_destroy (cursor);
    ShowError(_("Not a valid archive???!"));
    return;
  }
  
  arch->filetype = found; /*this could be changed by readit */
  
  gtk_clist_clear( GTK_CLIST (arch->listFiles));
  gtk_clist_freeze( GTK_CLIST(arch->listFiles));
  
  if (archList[found].readit(arch->OpenFile) == -1) {
    if (arch->OpenFile != NULL) {g_free(arch->OpenFile); arch->OpenFile = NULL;}
    clearStatusbar();
    cursor = gdk_cursor_new (0);
    gdk_window_set_cursor (window->window, cursor);
    gdk_cursor_destroy (cursor);
    ShowError(_("Error opening archive"));
    return;
  }

  for (i = 0; i < COLNUM; i++) {
    gtk_clist_set_column_width (GTK_CLIST (arch->listFiles),i,gtk_clist_optimal_column_width (GTK_CLIST (arch->listFiles),i));
  }
  
  gtk_clist_thaw( GTK_CLIST (arch->listFiles));
  
  title = g_malloc(sizeof(gchar)*(8+strlen(arch->OpenFile)+1));
  strcpy(title, "gxTar - ");
  strcat(title, arch->OpenFile);
  gtk_window_set_title (GTK_WINDOW (window), title);
  g_free(title);

  clearStatusbar();
  cursor = gdk_cursor_new (0);
  gdk_window_set_cursor (window->window, cursor);
  gdk_cursor_destroy (cursor);

  arch->Blank = FALSE;
}


/************* Below are the "public" functions that archive.h defines *********/

void openfileArchive() {

  closeArchive();
  create_file_selection();
}


void closeArchive(){

  gtk_clist_clear( GTK_CLIST (arch->listFiles));
  if (arch->OpenFile != NULL) {
    g_free(arch->OpenFile); 
    arch->OpenFile = NULL;
  }
  gtk_window_set_title (GTK_WINDOW (window), "gxTar - Unknown");

}

void addArchive(){

  if (arch->OpenFile == NULL) {
    ShowError(_("There is no currently open archive."));
    return;
  }
  create_add_selection();
}


void doDelete (GtkWidget *widget, gpointer *stuff) {
  
  setStatusbar(_("Deleting Files..."));
  while (gtk_events_pending()) gtk_main_iteration();
  
  archList[arch->filetype].deleteit();
  
  /*find a better way to do this! */
  openArchive();
  clearStatusbar();
}

void deleteArchive() {

  GtkWidget * mbox;
  
  if (arch->OpenFile == NULL)
    {
      ShowError(_("There is no currently open archive."));
      return;
    }

  if (GTK_CLIST(arch->listFiles)->selection == NULL) {
    ShowError("No files selected!");
    return ;
  }

  if (PromptOnDelete) {
    mbox = gnome_message_box_new(_("Are you sure you want to delete these files?"),
                                 GNOME_MESSAGE_BOX_QUESTION,
                                 GNOME_STOCK_BUTTON_YES,
                                 GNOME_STOCK_BUTTON_NO, NULL);
    
    gnome_dialog_button_connect (GNOME_DIALOG (mbox), 0, doDelete, NULL);
    gtk_widget_show(mbox);
  }
  else {
    doDelete(NULL,NULL );
  }
}

void extractArchive() {

  if ((arch->OpenFile != NULL)&&(!arch->Blank))
    create_extract_selection();
  else
    ShowError("No archive open or no files to extract.");

}


void doQuit(GtkWidget *widget, gpointer *stuff) {
  gdk_window_get_position(window->window,&os_x,&os_y);
  gdk_window_get_size(window->window,&os_w,&os_h);
  
  gnome_config_push_prefix ("gxtar/");
  
  if (RememberPosition) {

  
    gnome_config_set_int("Geometry/x",  os_x);
    gnome_config_set_int("Geometry/y",  os_y);
    gnome_config_set_int("Geometry/w",  os_w);
    gnome_config_set_int("Geometry/h",  os_h);
  }
  gnome_config_set_string("Options/LastEDir",   LastEDir);
  gnome_config_set_string("Options/LastODir",   LastODir);
  gnome_config_sync();
  gnome_config_pop_prefix();

  gtk_main_quit ();
}

void quitArchive () {/*our general clean up and exit routine*/

  GtkWidget * mbox;

  if (PromptOnQuit) {
    /*create dialog to do delete*/
    mbox = gnome_message_box_new(_("Are you sure you want to exit?"),
                                 GNOME_MESSAGE_BOX_QUESTION,
                                 GNOME_STOCK_BUTTON_YES,
                                 GNOME_STOCK_BUTTON_NO, NULL);
    
    gnome_dialog_button_connect (GNOME_DIALOG (mbox), 0, doQuit, NULL);
    gtk_widget_show(mbox);
  }
  else {
    doQuit(NULL,NULL);
  }
}

void selectAll(){
 gtk_clist_select_all(GTK_CLIST(arch->listFiles));
}

void deselectAll(){
  gtk_clist_unselect_all(GTK_CLIST(arch->listFiles));
}

void newfile_selection_hide_fileops (GtkWidget *widget, GtkFileSelection *fs) {
  gtk_file_selection_hide_fileop_buttons (fs);
}

void newfile_selection_ok (GtkWidget *widget, GtkFileSelection *fs) {
  
  const gchar * mimetype;
  int i;
  int found = -1;
  gchar *title;

  /*get file name*/

  if (arch->OpenFile != NULL) {g_free(arch->OpenFile);  arch->OpenFile = NULL;}
  arch->OpenFile = g_malloc0(strlen(gtk_file_selection_get_filename (GTK_FILE_SELECTION (fs)))+1);
  strcpy(arch->OpenFile, gtk_file_selection_get_filename (GTK_FILE_SELECTION (fs)));
  gtk_object_destroy (GTK_OBJECT (fs));

  if (arch->OpenFile[strlen(arch->OpenFile)-1] == '/') {
    /*complain that they gave us a directory name*/
    ShowError("You must specify the name of a new archive to create.");
    if (arch->OpenFile) {g_free(arch->OpenFile); arch->OpenFile = NULL;}
    return;
  }

  /*warn if it exists*/

  /*make sure we can fgiure out what to use*/
 
  mimetype = gnome_mime_type (arch->OpenFile);
  if (!debug_off) g_print("mime:%s\n",mimetype);

  /*check against our known mimetypes*/

  for (i = 0; i < KNOWNEXT; i++) 
    if (strcmp(mimetype, archList[i].mimetype)==0) found = i;

  if (found == -1) {
    ShowError("Unknown archive type, cannot create");
    if (arch->OpenFile) {g_free(arch->OpenFile); arch->OpenFile = NULL;}
    return;
  }
  
  /*now get the path from Openfile and save it for later*/
  if (LastODir != NULL) g_free(LastODir);
  LastODir =   splitNamePath(arch->OpenFile,1);


  title = g_malloc(sizeof(gchar)*(8+strlen(arch->OpenFile)+1));
  strcpy(title, "gxTar - ");
  strcat(title, arch->OpenFile);
  gtk_window_set_title (GTK_WINDOW (window), title);
  g_free(title);

  arch->filetype = found;
  arch->Blank = TRUE;
  /*not much of a point to not add files now*/
  addArchive();
}

void create_newfile_selection () {
  static GtkWidget *window = NULL;
  GtkWidget *button;
  
  if (!window)
    {
      window = gtk_file_selection_new (_("New Archive"));
      gtk_file_selection_hide_fileop_buttons (GTK_FILE_SELECTION (window));
      gtk_window_position (GTK_WINDOW (window), GTK_WIN_POS_MOUSE);
      gtk_signal_connect (GTK_OBJECT (window), "destroy",
                          GTK_SIGNAL_FUNC(gtk_widget_destroyed),
                          &window);
      gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (window)->ok_button),
                          "clicked", GTK_SIGNAL_FUNC(newfile_selection_ok),
                          window);
      gtk_signal_connect_object (GTK_OBJECT (GTK_FILE_SELECTION (window)->cancel_button),
                                 "clicked", GTK_SIGNAL_FUNC(gtk_widget_destroy),
                                 GTK_OBJECT (window));
      button = gtk_button_new_with_label (_("Hide Fileops"));
      gtk_signal_connect (GTK_OBJECT (button), "clicked",
                          (GtkSignalFunc) newfile_selection_hide_fileops, 
                          (gpointer) window);
      gtk_box_pack_start (GTK_BOX (GTK_FILE_SELECTION (window)->action_area), 
                          button, FALSE, FALSE, 0);
      gtk_widget_show (button);
      button = gtk_button_new_with_label (_("Show Fileops"));
      gtk_signal_connect_object (GTK_OBJECT (button), "clicked",
                                 (GtkSignalFunc) gtk_file_selection_show_fileop_buttons, 
                                 (gpointer) window);
      gtk_box_pack_start (GTK_BOX (GTK_FILE_SELECTION (window)->action_area), 
                          button, FALSE, FALSE, 0);
      gtk_widget_show (button);

      if (LastODir != NULL) gtk_file_selection_set_filename (GTK_FILE_SELECTION(window), LastODir);

      gtk_widget_hide(GTK_FILE_SELECTION(window)->file_list);
      gtk_widget_hide(GTK_FILE_SELECTION(window)->file_list->parent);

    }
  if (!GTK_WIDGET_VISIBLE (window))
    {
      gtk_widget_show (window);
      gtk_grab_add(window);
    }
  else
    {
      gtk_widget_destroy (window);
      gtk_grab_remove(window);
    }
}

void newArchive(){

  closeArchive();
  create_newfile_selection();
}
