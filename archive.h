/* gxTar - Gnomified Archive Frontend
 * Copyright (C) 1998 Chris Rogers
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option) 
 * any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  
 * 02111-1307, USA.
 */

/* archive.h - contains basic archive functions */
/* right now we must interact with the cli programs, perhaps someday we can implement the (de)compression ourslef */

#ifndef __ARCHIVE_H__
#define __ARCHIVE_H__

#include "config.h"
#include <gnome.h>

/*global "arch" object maybe not the best way to go, but i got tired of passing this all around */

struct archive
{
  gchar *OpenFile; 
  GtkWidget *listFiles;
  gint filetype;
  gint Blank;
} * arch;



/*Config Options */
int os_x, os_y, os_w,os_h;
int RememberPosition;
int PromptOnDelete;
int PromptOnQuit;
gchar * ForcedTar;
gchar *LastODir;
gchar *LastEDir;
gint debug_off;
int less_w, less_h;

gchar *tarl;
gchar *gzipl;
gchar *gunzipl;
gchar *zipl;
gchar *bzip2l;
gchar *bunzip2l;
gchar *unarjl;

/*top level window*/
GtkWidget *window;

void openfileArchive();
void openArchive();
void extractArchive();
void addArchive();
void closeArchive();
void quitArchive();
void newArchive();
void deleteArchive();
void viewFile();
void selectAll();
void deselectAll();

#endif /* __ARCHIVE_H__ */
