/* gxTar - Gnomified Archive Frontend
 * Copyright (C) 1998 Chris Rogers
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option) 
 * any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  
 * 02111-1307, USA.
 */

#include "tar.h"


/*these guys get a file name and unatars it and fills in the clist */

int tar_read(gchar * fle) {

  gchar *text[COLNUM];
  gchar tmp[6][1024];
  FILE * fl;
  gchar *cmd;
  int removeline;

  cmd = g_malloc(sizeof(gchar)*(9+strlen(fle)+strlen(tarl)));
  sprintf(cmd, "%s -tvf \"%s\"",tarl, fle);
 
  if ((fl = popen(cmd,"r")) == NULL) {
    return -1 ;
  }
  g_free(cmd);

  removeline = 0;
  while (!feof(fl))  
    {
      fscanf(fl,"%s %s %s %s %s",tmp[0],tmp[1],tmp[2],tmp[3],tmp[4]);
      text[3] = tmp[0];
      text[2] = tmp[2];
      text[1] = tmp[3];
      text[4] = NULL;

      fgets(tmp[5],1023,fl);
      /*strip the /n. also below, skip the first space*/
      tmp[5][strlen(tmp[5])-1] = '\0';

      text[0] = splitNamePath(tmp[5]+1,0);
      text[5] = splitNamePath(tmp[5]+1,1);
      
      gtk_clist_append( GTK_CLIST (arch->listFiles) , text);
      removeline++;
      g_free(text[0]);
      g_free(text[5]);
}

  /*we get an extra line for some reason???*/
  gtk_clist_remove (GTK_CLIST (arch->listFiles),removeline-1); 

  pclose(fl);
  return 0; 
}

int  tar_extract(gchar * dir){

  GList *selection;
  gint index;
  gchar *text;
  gchar *tmpOut;
  gchar *cmd;
  gint result;
  FILE * src;

  selection = GTK_CLIST(arch->listFiles)->selection;
  tmpOut = NULL;
  
  if (selection == NULL) {
    cmd = g_malloc(sizeof(gchar)*(15+strlen(tarl)+strlen(dir)+strlen(arch->OpenFile)));
    sprintf(cmd, "cd \"%s\"; %s -xf \"%s\"", dir, tarl,  arch->OpenFile);
  }else{
    tmpOut = tempnam("","gxtar");
    src = fopen(tmpOut,"w");
    
    while(selection) {
      index = (gint) selection->data;
      gtk_clist_get_text(GTK_CLIST(arch->listFiles), index, 0, &text);
      if (!strchr(text,'/')) {
	gtk_clist_get_text(GTK_CLIST(arch->listFiles), index, COLNUM-1, &text);
	fputs(text,src); 
	gtk_clist_get_text(GTK_CLIST(arch->listFiles), index, 0, &text);
	fputs(text,src); 
	fputc('\n',src);
      }
      selection= selection->next;
    }
    fclose(src);
    cmd = g_malloc(sizeof(gchar)*(19+strlen(tarl)+strlen(dir)+strlen(arch->OpenFile)+strlen(tmpOut)));
    sprintf(cmd, "cd \"%s\"; %s -xf \"%s\" -T %s", dir, tarl, arch->OpenFile, tmpOut);
    }
  
  result = system(cmd); 
  if (tmpOut != NULL) {
    unlink(tmpOut);  
    free(tmpOut);
  }
  g_free(cmd);
  return result;
}


int tar_add_actual(gchar *src, GtkFileSelection *fs){

  gchar *cmd;
  gint nullnum;
  int result;
  gchar * fle;
  gchar * text;
  gchar * to;
  gchar * tmpOut;
  FILE * tfle;
  int index;
  GList * selection;

 
  fle = g_malloc0(strlen(gtk_file_selection_get_filename (GTK_FILE_SELECTION (fs)))+1);
  strcpy(fle, gtk_file_selection_get_filename (GTK_FILE_SELECTION (fs)));

  to = tempnam("","gxtar");
  tmpOut = g_malloc(sizeof(gchar) * (strlen(to)+1));
  strcpy(tmpOut,to);

  tfle = fopen(tmpOut,"w");

  selection = GTK_CLIST (fs->file_list)->selection;
  if (selection) {
    while(selection) {
      index = (gint) selection->data;
      gtk_clist_get_text(GTK_CLIST(fs->file_list), index, 0, &text);
      fputs(LastODir,tfle);
      fputs(text,tfle); 
      fputc('\n',tfle);
      selection= selection->next;
    }
  }
  else {
    fputs(gtk_file_selection_get_filename (GTK_FILE_SELECTION (fs)),tfle);
    fputc('\n',tfle);
  }

  fclose(tfle);

  if (debug_off) nullnum = 14; else nullnum = 0;
  
  if (arch->Blank) {
    cmd = g_malloc(sizeof(gchar)*(14+strlen(tarl)+strlen(src)+strlen(tmpOut)+nullnum));
    sprintf(cmd, "%s -cf \"%s\" -T %s", tarl, src, tmpOut);
  } else {
    cmd = g_malloc(sizeof(gchar)*(14+strlen(tarl)+strlen(src)+strlen(tmpOut)+nullnum));
    sprintf(cmd, "%s -rf \"%s\" -T %s", tarl, src, tmpOut);
  }
  if (debug_off) strcat(cmd, " >> /dev/null");
  result =  system(cmd);
  g_free(cmd);

  if (tmpOut != NULL) {
    unlink(tmpOut);  
    free(tmpOut);
  }

  return  result;
}

int tar_add(GtkFileSelection *fs){
  return tar_add_actual(arch->OpenFile,fs);
}

int tar_delete_actual(gchar *src, gchar *fle) {

  GList *selection;
  gint index;
  gchar *text;
  gchar *cmd;
  gint sze;
  int result;
  
  /*we have to do this twice to get the size*/
  if (debug_off) sze = 16 + 14 + strlen(src) +strlen(tarl); else sze = 16 + strlen(src)+strlen(tarl);

  selection = GTK_CLIST(arch->listFiles)->selection;
  while(selection) {
    index = (gint) selection->data;
    /*strcat(cmd," \"");*/ sze = sze + 2;
    gtk_clist_get_text(GTK_CLIST(arch->listFiles), index, COLNUM-1, &text);
    /*strcat(cmd,text);*/  sze = sze + strlen(text);
    gtk_clist_get_text(GTK_CLIST(arch->listFiles), index, 0, &text);
    /*strcat(cmd,text);*/  sze = sze + strlen(text);
    /*strcat(cmd,"\"");*/ sze = sze +1;
    selection= selection->next;
  }

  cmd = g_malloc(sizeof(gchar)*sze);
  sprintf(cmd,"%s --delete -f \"%s\"", tarl, src);

  selection = GTK_CLIST(arch->listFiles)->selection;
  while(selection) {
    index = (gint) selection->data;
    strcat(cmd," \"");
    gtk_clist_get_text(GTK_CLIST(arch->listFiles), index, COLNUM-1, &text);
    strcat(cmd,text); 
    gtk_clist_get_text(GTK_CLIST(arch->listFiles), index, 0, &text);
    strcat(cmd,text); 
    strcat(cmd,"\"");
    selection= selection->next;
  }

  if (debug_off) strcat(cmd, " >> /dev/null");  
  result =  system(cmd); 
  g_free(cmd);
  return result; 
}

int tar_delete(gchar *fle){
  return tar_delete_actual(arch->OpenFile, fle);
}

gchar * tar_view( gchar *fle){
  gchar * cmd;

  cmd = g_malloc(sizeof(gchar)*(12+strlen(fle)+strlen(arch->OpenFile)+strlen(tarl)));
  sprintf(cmd, "%s -xOf \"%s\" \"%s\"",tarl, arch->OpenFile, fle);
  return cmd;
}


