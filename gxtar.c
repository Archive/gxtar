/* gxTar - Gnomified Archive Frontend
 * Copyright (C) 1998 Chris Rogers
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option) 
 * any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  
 * 02111-1307, USA.
 */


#include <config.h> 
#include <gnome.h>
#include "menu.h"
#include "toolbar.h"
#include "archive.h"
#include "filelist.h"
#include "statusbar.h"


int restarted = 0;
gchar *openedfile;
gchar **initial_file;

enum {
  DEBUGON_KEY  = -1,
};


/* True if parsing determined that all the work is already done.  */
int just_exit = 0;

/* Session management */
static gint save_state (GnomeClient        *client,
			gint                phase,
			GnomeRestartStyle   save_style,
			gint                shutdown,
			GnomeInteractStyle  interact_style,
			gint                fast,
			gpointer            client_data)
{
  gchar *session_id;
  gchar *argv[3];
  gint x, y, w, h;
  
  session_id= gnome_client_get_id (client);
  

  gdk_window_get_geometry (window->window, &x, &y, &w, &h, NULL);
  gnome_config_push_prefix (gnome_client_get_config_prefix (client));
  gnome_config_set_int ("Geometry/x", x);
  gnome_config_set_int ("Geometry/y", y);
  gnome_config_set_int ("Geometry/w", w);
  gnome_config_set_int ("Geometry/h", h);
  if (arch->OpenFile)  gnome_config_set_string("Saved/OpenFileName",arch->OpenFile);

  gnome_config_pop_prefix ();
  gnome_config_sync();

  argv[0] = (char*) client_data;
  argv[1] = "--discard-session";
  argv[2] = gnome_client_get_config_prefix (client);
  gnome_client_set_discard_command (client, 3, argv);
  gnome_client_set_clone_command (client, 1, argv);
  gnome_client_set_restart_command (client, 1, argv);

  return TRUE;
}


static gint die (GnomeClient *client, gpointer client_data)
{
  gtk_exit (0);
  return FALSE;
}


/*our general clean up and exit routine*/
void delete_event (GtkWidget *widget, gpointer *data) {
  quitArchive();
}

static void parse_an_arg (poptContext state,enum poptCallbackReason reason, const struct poptOption *opt, const char *arg, void *data)
{
  
  int key = opt->val;
  
  switch (key){
    
  case DEBUGON_KEY:
    debug_off = FALSE;
    break; 
    
  default:
  }
}


static struct poptOption cb_options [] = {
  { NULL, '\0', POPT_ARG_CALLBACK, parse_an_arg, 0},
  
  { "debug", '\0', POPT_ARG_NONE, NULL, DEBUGON_KEY,
    N_("Allows tar,gzip,etc to spew on the console."), NULL},
  
  { NULL, '\0', 0, NULL, 0}
};

void prepare_app() {

  GtkWidget *frame;
  GtkWidget *fTop;
  GtkWidget *fBottom;

  window = gnome_app_new ("gxtar", _("gxTar - Unknown") );

  gtk_widget_realize (window);
  gtk_signal_connect (GTK_OBJECT (window), "delete_event",
		      GTK_SIGNAL_FUNC (delete_event), NULL);

  /*Allow ourselves to be grown and shrunk */
  gtk_window_set_policy( GTK_WINDOW (window),TRUE,TRUE,TRUE);

  /*get this info from the config*/
  gnome_config_push_prefix ("gxtar/");
  RememberPosition = gnome_config_get_int ("Options/RememberPosition=1");
  
  if ((RememberPosition) || (restarted))  {
    os_x = gnome_config_get_int ("Geometry/x=20");
    os_y = gnome_config_get_int ("Geometry/y=20");
    os_w = gnome_config_get_int ("Geometry/w=400");
    os_h = gnome_config_get_int ("Geometry/h=400");
    gtk_window_set_default_size(GTK_WINDOW (window), os_w, os_h);
    gtk_widget_set_uposition (window, os_x, os_y);
  }
  else {
    gtk_window_set_default_size(GTK_WINDOW (window), 400, 400);
    gtk_widget_set_uposition (window, 20, 20);
  }
  PromptOnDelete = gnome_config_get_int ("Options/PromptOnDelete=0");
  PromptOnQuit = gnome_config_get_int ("Options/PromptOnQuit=0");
  ForcedTar = gnome_config_get_string("Options/ForcedTar");
  LastODir = gnome_config_get_string("Options/LastODir");
  LastEDir = gnome_config_get_string("Options/LastEDir");

  tarl =    gnome_config_get_string_with_default("Options/TarLocation=tar",NULL);
  gzipl =   gnome_config_get_string_with_default("Options/GzipLocation=gzip",NULL);
  gunzipl = gnome_config_get_string_with_default("Options/GunzipLocation=gunzip",NULL);
  zipl =    gnome_config_get_string_with_default("Options/ZipLocation=zip",NULL);
  bzip2l =  gnome_config_get_string_with_default("Options/Bzip2Location=bzip2",NULL);
  bunzip2l =gnome_config_get_string_with_default("Options/Bunzip2Location=bunzip2",NULL);
  unarjl =  gnome_config_get_string_with_default("Options/UnarjLocation=unarj",NULL);

  less_w = gnome_config_get_int ("Options/Less_W=400");
  less_h = gnome_config_get_int ("Options/Less_H=400");
  gnome_config_pop_prefix();

  /*end info from config*/

  gtk_container_border_width (GTK_CONTAINER (window), 1 );
      
  /*create the containers*/
  frame = gtk_vbox_new(FALSE, 0);
  fTop = gtk_hbox_new(FALSE, 0);
  fBottom = gtk_hbox_new(FALSE, 0);
  
  gtk_container_border_width(GTK_CONTAINER( frame), 1);
  
  gnome_app_set_contents ( GNOME_APP (window), frame);
  createMenu(window);
  createToolbar(window);
  
  gtk_container_border_width(GTK_CONTAINER( fBottom), 7);
  gtk_box_pack_start(GTK_BOX(frame), fBottom, TRUE, TRUE, 0);
  createFileList(fBottom);
  createStatusbar(frame);

  /*show it all*/
  
  gtk_widget_show(fBottom);
  gtk_widget_show(frame);
  gtk_widget_show(window);
}



/**********************MAIN********************************/

/*Create the widgets and go*/
int main (int argc, char *argv[]) {
  
  GnomeClient *client;
  poptContext ctx;

  /* Initialize the i18n stuff */
  bindtextdomain (PACKAGE, GNOMELOCALEDIR);
  textdomain (PACKAGE);

  /* Init Stuff that will be updated by opts */
  initial_file = NULL;
  debug_off = TRUE;
  
  gnome_init_with_popt_table("gxtar", VERSION, argc, argv,cb_options, 0, &ctx);
  client= gnome_master_client();

  /* Arrange to be told when something interesting happens.  */
  gtk_signal_connect (GTK_OBJECT (client), "save_yourself",
                      GTK_SIGNAL_FUNC (save_state), (gpointer) argv[0]);
  gtk_signal_connect (GTK_OBJECT (client), "die",
                      GTK_SIGNAL_FUNC (die), NULL);
  
 
  /*clone stuff, still not sure how all this works */
  /*if (GNOME_CLIENT_CONNECTED (client))
    {
      
      GnomeClient *cloned= gnome_cloned_client ();
      
      if (cloned)
        {
          restarted= 1;
          gnome_config_push_prefix (gnome_client_get_config_prefix (cloned));
          os_x = gnome_config_get_int ("Geometry/x");
          os_y = gnome_config_get_int ("Geometry/y");
          os_w = gnome_config_get_int ("Geometry/w");
          os_h = gnome_config_get_int ("Geometry/h");
	  openedfile = gnome_config_get_string("Saved/OpenFileName");
          gnome_config_pop_prefix ();
        }
    }
  */
  if (! just_exit)
    {

      prepare_app();

      if (argc > 1) {
	arch->OpenFile = g_malloc( ( strlen(argv[1])+1) *sizeof(gchar) );
	strcpy(arch->OpenFile,argv[1]);
	printf(">%s<\n",arch->OpenFile);
      }
      else if (openedfile != NULL) 
	{
	  arch->OpenFile = g_malloc( ( strlen( openedfile )+1) *sizeof(gchar) );
	  strcpy(arch->OpenFile, openedfile);
	  g_free(openedfile);
	}
      else arch->OpenFile = NULL;

      if (arch->OpenFile !=NULL) openArchive();
      gtk_main();
    }
  
  return 0;
}
