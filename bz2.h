/* gxTar - Gnomified Archive Frontend
 * Copyright (C) 1998 Chris Rogers
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option) 
 * any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  
 * 02111-1307, USA.
 */


#ifndef __BZ2_H__
#define __BZ2_H__

#include "archive.h"
#include "archivetypes.h"
#include <stdio.h>
#include <string.h>
#include "error.h"
#include "statusbar.h"

/*define all of the gz functions */

int bz2_read(gchar * fle);
int bz2_extract(gchar *dir);
int bz2_add(GtkFileSelection *fs);
int bz2_delete();
gchar * bz2_view(gchar * fle);

#endif /* __BZ2_H__ */
