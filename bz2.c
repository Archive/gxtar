/* gxTar - Gnomified Archive Frontend
 * Copyright (C) 1998 Chris Rogers
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option) 
 * any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  
 * 02111-1307, USA.
 */

#include "bz2.h"


int bz2_read(gchar * fle) {

  gchar *ext;
  gchar *savedot;
 
 /*ok first we want to check for the possibility of a tar.bz2 file.*/
  ext = strrchr(fle, '.');
  savedot = ext;
  ext[0] = '\0';
  if ((ext = strrchr(fle, '.')) != NULL) /*look for the next to last .*/
      if (strcmp(ext, ".tar")==0) {
	savedot[0] = '.'; /* put it back */
	arch->filetype = TBZ2;
	return archList[arch->filetype].readit(fle);
      }
  savedot[0] = '.'; /* put it back */

  /*hmm seems there is no way to view the contents of a bzip2 archive. */
return -1;
}

int  bz2_extract(gchar * dir){
  return -1;
}

int bz2_add(GtkFileSelection *fs){
  return -1;
}

int bz2_delete(gchar * cmd, GList * sltn){
  cmd[0] = '\0'; 
  return -1;
}

gchar * bz2_view(gchar *fle){
  return 0;
}
